package script.textosTunki;

public class Textos {
	public static final String RECIBISTE_UN_COBRO = "Recibiste un cobro";
	public static final String RECIBISTE_UN_PAGO = "Recibiste un pago";
	public static final String ENVIASTE_UN_COBRO = "Enviaste un cobro";
	public static final String ENVIASTE_UN_PAGO = "Enviaste un pago";
	public static final String TARJETA_YA_ESTA_VINCULADA = "Esa cuenta ya está vinculada.";
	public static final String UPS = "¡Ups! Hubo un error, por favor intenta de nuevo";
	public static final String ERROR_INESPERADO = "Ha ocurrido un error inesperado";
	public static final String USUARIO_NO_EXISTE_CAMBIO_CLAVE = "No hemos encontrado tu registro. Por favor crea una cuenta Tunki o escríbenos a tunki@intercorp.com.pe para ayudarte.";
	public static final String PAGO_REALIZADO = "¡Pago realizado!";
	public static final String ENVIAR = "ENVIAR";
	public static final String RESTABLECE_TU_CONTRASEÑA = "Restablece tu contraseña";
	public static final String DETALLE_RESTABLECE_TU_CONTRASEÑA = "Tu cuenta ha sido temporalmente bloqueada por intentos fallidos. Para acceder a Tunki debes crear una nueva contraseña.";
	public static final String INGRESA_TU_CUENTA = "Ingresa tu cuenta";
	public static final String ACEPTAR = "Aceptar";
	public static final String CANCELAR = "Cancelar";
	public static final String DESCRIPCION_INGRESA_TU_DOCUMENTO = "Ingresa tu número de documento asociado a Tunki";
	public static final String ADVERTENCIA_INGRESA_TU_DOCUMENTO = "Si tienes problemas, comunícate con nosotros a: tunki@intercorp.com.pe para ayudarte.";
	public static final String VALIDA_TU_CORREO = "Valida tu correo";
	public static final String DESCRIPCION_VALIDA_TU_CORREO = "Ingresa el código que recibiste en tu bandeja de entrada o correo no deseado:";
	public static final String REENVIAR_CODIGO = "Reenviar código";
	public static final String DESCRIPCION_CREA_TU_CONTRASEÑA = "Ingresa una contraseña nueva";
	public static final String ADVERTENCIA_CREA_TU_CONTRASEÑA = "Si tienes problemas, comunícate con nosotros a: tunki@intercorp.com.pe para ayudarte.";
	public static final String CONFIRMAR = "Confirmar";
	public static final String DESCRIPCION_CONTRASEÑA_CREADA = "	Tu contraseña se guardó correctamente";
	public static final String CONTINUAR = "Continuar";
	public static final String PASS_DIFERENTE = "No coinciden las contraseñas";
	public static final String NO_TIENES_TICKECTS = "Por el momento no tienes tickets";
	// textos para validación de item en menu mas
	public static final String VINCULA_TARJETA = "Vincula tu tarjeta";
	public static final String MIS_TICKETS = "Mis Tickets";
	public static final String BENEFICIOS_TUNKI = "Beneficios Tunki";
	public static final String CONFIGURACION = "Configuración";
	public static final String SUGERIR_COMERCIO = "Sugerir un comercio";
	public static final String CONTACTANOS = "Contáctanos";
}
