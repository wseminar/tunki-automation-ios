package scripts.negocio;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusableDevolucionComercio;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class Cobro {
	
	private static String msjFinal = "";

	public static void cobro() throws Exception{
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
			
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		if(!buscarClientes(GlobalData.getData("viClienteNombre"), true)){ 
			msjFinal = "Error: No se encontró el cliente.";
			return;
		}
		
		if(GlobalData.getData("viCobros").equals("NO")){
			while(!buscarClientes(GlobalData.getData("viClienteNombre"), false));
			return;
		}			
		
		if(!EnvioCobro()) return;
				
		if(GlobalData.getData("viCancelarCobro").equals("SI")){
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CANCEL.getValue()));			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));			
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty()) return;
			
			if(GlobalData.getData("viReCobro").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
				
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				
				if(!buscarClientes(GlobalData.getData("viClienteNombre"), true)){
					msjFinal = "Error: No se encontró el cliente.";
					return;
				}
				
				if(!EnvioCobro()) return;
			}
		}
		
		int tries = 0;
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WAITING.getValue())).toString())) {
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty()) return;
			
			if(tries == 5) {
				msjFinal = "La persona no acepto la solicitud de cobro.";
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CANCEL.getValue()));
				return; 
			}
			tries++;
			
			Thread.sleep(30000);
		}
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_PAYMENT_ID.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetAttribute,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_TITTLE.getValue()), "label").toString();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			return;
		}
				
		if(GlobalData.getData("viConfirmacionCobro").equals("NO")) return;	
		Reutilizable.tomarCapturaPantalla(300, "Pago Realizado");
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));

		if(GlobalData.getData("viHistorialCobros").equals("NO")) return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));			
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.tomarCapturaPantalla(500, "Historial"); 
		TouchAction s = new TouchAction((PerformsTouchActions)EFA.cv_driver);
		s.tap(169, 155).perform();
		Reutilizable.tomarCapturaPantalla(300, "Constancia");
		if(GlobalData.getData("viRealizarDevolucion").toUpperCase().equals("SI")) {
			msjFinal = ReusableDevolucionComercio.Devolucion();
			if(!msjFinal.isEmpty())return;
		}
	}
	
	public static String getResult() throws Exception{
		
		if(!GlobalData.getData("viMensajeErrorCobro").equals("")) {
			if(GlobalData.getData("viMensajeErrorCobro").trim().equals(msjFinal.trim()))
				return Mensajes.SUCCESS;
			else
				return "Los mensajes no son iguales: " + msjFinal;
		}			
		
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}
	
	public static boolean EnvioCobro() throws Exception{
		Element txtMonto = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther");
		EFA.executeAction(Action.Click, txtMonto);
		EFA.executeAction(Action.SendKeys, txtMonto, GlobalData.getData("viMontoCobro"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_FACE.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SEND_COLLECT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.tomarCapturaPantalla(300, "Espera Cliente");
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return false;
		
		return true;
	}
	
	public static boolean buscarClientes(String nombreCliente,boolean clic) throws Exception{
				Thread.sleep(4000);
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BUSCADOR.getValue()), nombreCliente);
					//List<WebElement> filas = EFA.cv_driver.findElements(By.id("cellCustomer"));
//					for (WebElement item : filas) {
//						WebElement nombre = item.findElement(By.id("txt_name"));
//						String fullName = nombre.getText();
//						if(fullName.toUpperCase().equals(nombreCliente.toUpperCase())){
//							if(clic) item.click();
//							msjFinal = "";
//							return true;
//						}
//					}
				String name = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME.getValue())).toString();
				if(name.toUpperCase().equals(nombreCliente.toUpperCase())) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME.getValue()));
					return true;
				}
			msjFinal = "No se encontro el cliente";
		return false;
	}
	
	public static boolean validarClientesHistorial(String cliente) throws Exception{
		Element objCliente = null;
		WebElement objClienteNat = null;
		
		//List<WebElement> lista = EFA.cv_driver.findElements(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell"));
		List<WebElement> lista = EFA.cv_driver.findElements(By.xpath("//*[@type='XCUIElementTypeCell'][@visible='true']"));
		for (int i = 0; i < 10; i++) {
			Thread.sleep(1500);
			if(lista.size() <= 0)
				lista = EFA.cv_driver.findElements(By.xpath("//*[@type='XCUIElementTypeCell'][@visible='true']"));
			else
				break; 
		}
		
		if(lista.size() > 0) {
			String nombreCliente = lista.get(0).findElement(By.xpath(".//XCUIElementTypeStaticText[1]")).getText().trim();
			if(cliente.toUpperCase().equals(nombreCliente.toUpperCase())){
				lista.get(0).click();
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Comprobante", 0);
			}
		}
		else {
			return false;
		}
		
		
		Element imgRegresar = new Element("xpath", "//XCUIElementTypeButton[@label='backSpec']");
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgRegresar).toString())){
			msjFinal = "Error: No se pudo mostrar la constancia de pago en la pantalla";
			return false;
		}
		else{
			EFA.executeAction(Action.Click, imgRegresar);
			Reutilizable.tomarCapturaPantalla(500, "Movimientos");
			return true;
		}
	}
	
	public static boolean validarListaClientes() throws Exception{
		Element btnCerrarBusqueda = new Element("id", "search_close_btn");
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		EFA.executeAction(Action.Click, btnCerrarBusqueda);
		int nroClientes = EFA.cv_driver.findElements(By.id("txt_name")).size();
		if(nroClientes <= 0){
			return false;
		}
		return true;
	}
}
