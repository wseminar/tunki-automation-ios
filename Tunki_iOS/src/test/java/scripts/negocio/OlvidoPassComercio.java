package scripts.negocio;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.UtilitariosErrores;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class OlvidoPassComercio {
	private static String msjFinal = "";
	public static void PassComercio() throws Exception{
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue())).toString()));
		if(GlobalData.getData("viMantenerSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHECK_REMEMBER.getValue()));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_FORGOT_PASSWORD.getValue()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_SEND.getValue())).toString())){
				msjFinal = "No es la pantalla correcta";
				return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()),GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOCK.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EMAIL.getValue()),GlobalData.getData("viEmail"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOCK.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SEND.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return;
		msjFinal=UtilitariosErrores.validarErrorTextoComercio();
		if(!msjFinal.isEmpty()) return;
		if(GlobalData.getData("viEmail").equals("correoinvalido@gmail.com")){
			EFA.cs_getTestEvidence("Envio de correo", 800);
			return;
		}
		Thread.sleep(40000);
		String codigoVerificacion = Reutilizable.obtenerCodigoVerificacionAlfa();
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue())).toString())){
			msjFinal = "No es la pantalla correcta";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue())).toString())){
			msjFinal = "No muestra la pantalla de Login";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()),GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
		if(GlobalData.getData("viMantenerSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHECK_REMEMBER.getValue()));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()),codigoVerificacion);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getValue()));	
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return;
		msjFinal=UtilitariosErrores.validarErrorTextoComercio();
		if(!msjFinal.isEmpty()) return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_CONFIRMATION.getValue())).toString())){
			msjFinal = "No muesta pantalla Cambia tu contraseña temporal";
			return;
		}
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()),GlobalData.getData("viPass1"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOCK.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_CONFIRMATION.getValue()),GlobalData.getData("viPass2"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOCK.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONFIRM.getValue()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return;
		msjFinal=UtilitariosErrores.validarErrorTextoComercio();;
		if(!msjFinal.isEmpty()) return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue())).toString())){
			msjFinal = "No muestra la pantalla de cambio de contraseÑa correctamente.";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return;
		msjFinal=UtilitariosErrores.validarErrorTextoComercio();
		if(!msjFinal.isEmpty()) return;
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_NOW.getValue())).toString()))
		{
			if(GlobalData.getData("viIsMantenerSesion").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			}else {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_NOW.getValue()));
			}
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return;
		msjFinal=UtilitariosErrores.validarErrorTextoComercio();
		if(!msjFinal.isEmpty()) return;	
		if(GlobalData.getData("viFlujosRecuperarPass").toUpperCase().equals("PASS TEMPORAL")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SIGNOUT.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=UtilitariosErrores.validarErrorTextoComercio();
			if(!msjFinal.isEmpty()) return;
			EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()), GlobalData.getData("viUsuario"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_PASSWORD.getValue()),codigoVerificacion);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getValue()));	
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=UtilitariosErrores.validarErrorTextoComercio();
			if(!msjFinal.isEmpty()) return;
		}
	}
	
	public static String getResult(){
		String mensajeEsperado = GlobalData.getData("viMensajeErrorPass");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}else{
				return "El mensaje no es igual: " + msjFinal;
			}
		}
		
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}	

}
