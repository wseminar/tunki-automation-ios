package scripts.negocio;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.search.*;

import com.everis.*;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class Registro {

	private static String msjFinal = "";

	public static void RegistroNegocio() throws Exception {
		
		if(GlobalData.getData("viFlujo1").equals("SI")){
			msjFinal = "";
			flujo1();
			if(msjFinal.equals(Mensajes.SUCCESS)) msjFinal = "";
			if(!msjFinal.isEmpty()) return;
		}
		
		if(GlobalData.getData("viFlujo2").equals("SI")){
			msjFinal = "";
			flujo2();
			if(msjFinal.equals(Mensajes.SUCCESS)) msjFinal = "";
			if(!msjFinal.isEmpty()) return;
		}
		
		if(GlobalData.getData("viFlujo3").equals("SI")){
			msjFinal = "";
			flujo3();
			if(msjFinal.equals(Mensajes.SUCCESS)) msjFinal = "";
			if(!msjFinal.isEmpty()) return;
		}
	}
	
	public static String getResult() {
		if(!GlobalData.getData("viMensajeErrorRegistro").isEmpty()) {
			if(GlobalData.getData("viMensajeErrorRegistro").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		
		return msjFinal.isEmpty() ? Mensajes.SUCCESS:msjFinal;
	}
	
	public static String obtenerCodigoVerificacion() throws Exception {
		String codigo = "";
		String primerDato = "";

		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i < 1; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "[0-9][0-9][0-9][0-9][0-9][0-9]";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
	
	public static boolean ingresarCodigoVerificacion() throws Exception{
		if(!GlobalData.getData("viCodigo").isEmpty()){
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_CODE_1.getValue()), GlobalData.getData("viCodigo"));
		}
		else {
			String codigo = obtenerCodigoVerificacion();
			if(!codigo.isEmpty()) {
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_CODE_1.getValue()), codigo);
			}
			else {
				msjFinal = "No se pudo obtener el codigo de verificacion";
				return false;
			}
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Pantalla Retorno", 0);
		
		if(GlobalData.getData("viReIngreso").equals("SI")) {
			if(msjFinal.equals(Mensajes.CODIGOEXPIRADO)) {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
				msjFinal = "";
				String codigo = obtenerCodigoVerificacion();
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_CODE_1.getValue()), codigo);
			}
			else{
				return false;
			}
		}
		else {
			if(!msjFinal.isEmpty()) {
				return false;
			}
		}
		
		return true;
	}

	public static boolean flujo1() throws Exception{
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CREATE.getValue())).toString()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CREATE.getValue()));
		
		if(!GlobalData.getData("viComercio").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_COMMERCE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_COMMERCE.getValue()), GlobalData.getData("viComercio"));
		}
		
		if(!GlobalData.getData("viRazonSocial").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_BUSINESS_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_BUSINESS_NAME.getValue()), GlobalData.getData("viRazonSocial"));
		}
		
		if(!GlobalData.getData("viRuc").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_RUC_NUMBER.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_RUC_NUMBER.getValue()), GlobalData.getData("viRuc"));
		}
		
		try {
			int nroComercios = Integer.parseInt(GlobalData.getData("viNroLocal"));
			
			if(nroComercios > 1){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.MASUNLOCAL.getValue()));
			}
		} catch (Exception e) {
			msjFinal = "El número de comercios es incorrecto";
			return false;
		}
		TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		x.tap(182,375).perform();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return false;
		
		//Ingresar datos del administrador		
		if(!GlobalData.getData("viNombre").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_NAME.getValue()), GlobalData.getData("viNombre"));
		}
		
		if(!GlobalData.getData("viApellido").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_LAST_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_LAST_NAME.getValue()), GlobalData.getData("viApellido"));
		}
		
		if(!GlobalData.getData("viEmail").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_EMAIL.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_EMAIL.getValue()), GlobalData.getData("viEmail"));
		}
		
		if(!GlobalData.getData("viCelular").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_PHONE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_ADMIN_PHONE.getValue()), GlobalData.getData("viCelular"));
		}
				
		if(!GlobalData.getData("viTipoDocumento").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.LBL_DOCUMENT.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.OPT_TIPO_DOC.getValue()));
		}
		
		if(!GlobalData.getData("viDocIdentidad").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_ADMIN_DOC_NUMBER.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_ADMIN_DOC_NUMBER.getValue()), GlobalData.getData("viDocIdentidad"));
		}
		x.tap(163, 92).perform();
		
		if(GlobalData.getData("viEntrarLink").equals("SI")){
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_TERMS_CONDITION.getValue()));
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Tratamiento de datos", 0);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BACK_BLUE.getValue()));
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Datos de Administrador", 0);
		}
		
		if(GlobalData.getData("vilsAutorizarDatosPersonales").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHECK_COND_TERMS_USE.getValue()));			
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return false;
		
		//ingresar codigo de validacion
		
		if(!ingresarCodigoVerificacion()) return false;
		
		//ingreso de usuario y contraseña
		
		if(!GlobalData.getData("viUsuario").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()), GlobalData.getData("viUsuario"));
		}
		if(!GlobalData.getData("viPassword").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()), GlobalData.getData("viPassword"));
		}
		if(!GlobalData.getData("viRePassword").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CONFIRM_PASSWORD.getValue()), GlobalData.getData("viRePassword"));
		}
		
		x.tap(163, 92).perform();
		
		if(GlobalData.getData("viEntrarLink").equals("SI")){
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.TV_TERMS_CONDITIONS.getValue()));
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Terminos y condiciones", 0);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BACK_BLUE.getValue()));
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Datos de Usuario", 0);
		}
		
		if(GlobalData.getData("viExpirarUsuario").equals("SI")) {
			int tiempoExpirar = Integer.parseInt(GlobalData.getData("viTiempoExpirar"));
			Thread.sleep(tiempoExpirar * 1000);
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Pantalla Retorno", 0);
		if(!msjFinal.isEmpty()) {
			return false;
		}
		
		if(GlobalData.getData("viIrInicioSesion").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_GO_TO_LOGIN.getValue()));
			Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
			msjFinal = Login.getResult();
			
			if(!msjFinal.isEmpty()) {
				return false;
			}
		}
		else {
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())) {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla", 0);
				return true;
				
			}
			else {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla Error", 0);
				return false;
			}
			
		}
		
		return true;
	}
	
	public static boolean flujo2() throws Exception{
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD_LOCAL.getValue())).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD_LOCAL.getValue()));
		}
		else{
			Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
			msjFinal = Login.getResult();
			if(!msjFinal.equals(Mensajes.SUCCESS)) return false;
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD_LOCAL.getValue()));
		}
		
		msjFinal = "";
		
		if(!GlobalData.getData("viNombreLocal").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_STORE_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_STORE_NAME.getValue()), GlobalData.getData("viNombreLocal"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.REVIEW_LOCAL_CELL_CUSTOMER.getValue()));
		}
		
		if(GlobalData.getData("viAgregarLogo").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_GALLERY.getValue()));
			if(!agregarImagen()) return false;
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return false;
		
		if(!GlobalData.getData("viDireccionLocal").isEmpty()) {
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_STORE_ADDRESS.getValue()), GlobalData.getData("viDireccionLocal"));
			if(GlobalData.getData("viDireccionLocal").equals("U")) {
				
			}
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return false;
		
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Mapa", 0);
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		
		if(GlobalData.getData("viPropina").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_AGREED.getValue()));
		}
		else {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_DECLINE.getValue()));
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return false;
		
		if(GlobalData.getData("viIrInicioSesion").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_GO_TO_LOGIN.getValue()));
			Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
			msjFinal = Login.getResult();
			
			if(!msjFinal.isEmpty()) {
				return false;
			}
		}
		else {
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())) {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla", 0);	
				return true;
			}
			else {
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Pantalla Error", 0);
				return false;
			}			
		}
		
		return true;
		
	}

	public static boolean flujo3() throws Exception{
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK_CARD.getValue())).toString())) {
			Login.loginRegistro(Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue()));
			msjFinal = Login.getResult();
			if(!msjFinal.equals(Mensajes.SUCCESS)) return false;
		}
		
		switch (GlobalData.getData("viAccionCuentaNegocio").toUpperCase()) {
		case "ABRIR":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_ACCOUNT.getValue()));
			Thread.sleep(7000);
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_ACCOUNT.getValue())).toString())){
				EFA.cs_getTestEvidence("Navegador Safari", 0);
				return true;
			}
			else {
				EFA.cs_getTestEvidence("Error Click Asociar", 0);
				msjFinal = "No aparecio la pagina de interbank";
				return false;
			}
			
		case "ASOCIAR":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LINK_CARD.getValue()));
			break;
			
		default:
			msjFinal = "No se reconoce la acción ingresada (" + GlobalData.getData("viAccionCuenta") + ").";
			return false;
		}
		
		msjFinal = "";
		
		if(!GlobalData.getData("viCuentaBancaria").isEmpty()){
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_ACCOUNT_NUMBER.getValue()), GlobalData.getData("viCuentaBancaria"));
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.REGISTER_ACCOUNT_SPEC.getValue()));
		
		switch (GlobalData.getData("viTipoCuentaBancaria").toUpperCase()) {
		case "AHORROS":
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CUENTA_AHORROS.getValue()));
			break;

		case "CORRIENTE":
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(),EButtons.CUENTA_CORRIENTE.getValue()));
			break;
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_ACCEPT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(msjFinal.equals("Número de cuenta no válido."))
		{
			if(!GlobalData.getData("viReCuenta").isEmpty())
			{
				msjFinal = "";
				EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_ACCOUNT_NUMBER.getValue()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_ACCOUNT_NUMBER.getValue()), GlobalData.getData("viReCuenta"));
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.REGISTER_ACCOUNT_SPEC.getValue()));
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_ACCEPT.getValue()));
				
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));		
				msjFinal = Reutilizable.validarErrores();
			}
		}
		
		if(!msjFinal.isEmpty()) {
			return false;
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WELCOME.getValue())).toString())) {
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pantalla", 0);
			
		}
		else {
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pantalla Error", 0);
			return false;
		}
		
		if(GlobalData.getData("viEmpezar").toUpperCase().equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_START.getValue()));
			
			Thread.sleep(500);
			EFA.cs_getTestEvidence("PopUp Sesión Activa", 0);
			
			if(GlobalData.getData("viIsMantenerSesion").equals("SI")) {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_ACCEPT.getValue()));
			}
			else {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_NOW.getValue()));
			}
			
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
			
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Pantalla Más", 0);
			
			/*EFA.executeAction(Action.Click, mnConfiguracion);
			
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Menú Configuración", 0);
			
			String valorSesionActiva = EFA.executeAction(Action.GetAttribute, chkSesionActiva, "value").toString();
			valorSesionActiva = valorSesionActiva.equals("1")?"SI":"NO";
			if(!GlobalData.getData("viIsMantenerSesion").toUpperCase().equals(valorSesionActiva)) {
				msjFinal = "El valor de la sesión activa no es igual a la ingresada";
				return false;
			}*/
		}
		
		return true;
	}
	
	public static boolean agregarImagen() throws Exception {
		
		Element album = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]");
		Element foto = new Element("xpath", "//*[@type='XCUIElementTypeCell'][@visible='true'][1]");
		Element btnDone = new Element("xpath", "//XCUIElementTypeButton[@label='Done']");
		Element btnCancel = new Element("xpath", "//XCUIElementTypeButton[@label='Cancel']");
				
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, album).toString())) {
			EFA.executeAction(Action.Click, album);
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, foto).toString())) {
				EFA.executeAction(Action.Click, foto);
				EFA.executeAction(Action.Click, btnDone);
			}
			else {
				msjFinal = "No hay ninguna imagen en el dispositivo";
				return false;
			}
		}
		else {
			msjFinal = "No hay ninguna imagen en el dispositivo";
			return false;
		}
		return true;
	}
}
