package scripts.negocio;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;

public class Devolucion {

	private static String msjFinal = "";
													 	
	public static void devolucion() throws Exception{
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue())).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
		 
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		Element btnMontoDev = new Element("xpath","//XCUIElementTypeTable[@name='recycler_view']/XCUIElementTypeCell[1]");
		EFA.executeAction(Action.Click, btnMontoDev);
		Reutilizable.tomarCapturaPantalla(500, "Constancia");
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));

		Element txtclave = new Element("xpath","//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSecureTextField");		
		Element btnAceptarDevolucion = new Element("xpath", "//XCUIElementTypeButton[@label='Aceptar']");
		//Element btncancel = new Element("xpath", "//XCUIElementTypeButton[@label='Cancelar']");
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.REPAYMENT.getValue())).toString())){
			msjFinal = "Error: Problemas en Devolución";
			return;
		}
		
		EFA.executeAction(Action.Click, txtclave);
		EFA.executeAction(Action.SendKeys, txtclave,GlobalData.getData("viPasswordDev"));
		EFA.executeAction(Action.Click, btnAceptarDevolucion);
		
		Element txtInputError = new Element("xpath","//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeStaticText");
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, txtInputError).toString())){
			Object inputError = EFA.executeAction(Action.GetAttribute, txtInputError, "label");
			
			if(inputError != null){
				msjFinal = inputError.toString();
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TXT_CANCEL.getValue()));
				return;
			}
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,  Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.tomarCapturaPantalla(500, "Historial");
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return ;
		
		Element btnMontoDev2 = new Element("xpath","//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]");
		EFA.executeAction(Action.Click, btnMontoDev2);
		Reutilizable.tomarCapturaPantalla(500, "Constancia");		
	}
	
	public static String getResult() throws Exception{
		
		if(!GlobalData.getData("viMensajeErrorDevolucion").isEmpty()){
			if(GlobalData.getData("viMensajeErrorDevolucion").equals(msjFinal)){
				return Mensajes.SUCCESS;
			}
			else
				return "Mensajes no iguales: " + msjFinal;
		}
		
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}
	
	public static boolean buscarCobro(float montoTotal, String persona) throws Exception {
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue())).toString())){
			int tries = 0;
			Thread.sleep(2000);
			List<WebElement> filas = EFA.cv_driver.findElementsByXPath("/XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell");
			if(tries < 5 && filas.size() == 0) {
				Thread.sleep(2000);
				filas = EFA.cv_driver.findElementsByXPath("/XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell");
				tries++;
			}
			Element btnEntendido = new Element("xpath", "//XCUIElementTypeButton[@name='backSpec']");
			if(filas.size() > 0) {
				for (WebElement webElement : filas) {
					boolean visibleCelda = Boolean.parseBoolean(webElement.getAttribute("visible"));
					while(!visibleCelda){
						hacerSwipe();
						visibleCelda = Boolean.parseBoolean(webElement.getAttribute("visible"));
					}
					
					String nombre = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[1]")).getAttribute("label");
					String fecha = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[2]")).getAttribute("label");
					String monto = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[3]")).getAttribute("label");
					if(fecha.contains(" S/ ")){
						nombre = fecha;
						fecha = monto;
						
					}
					Date actual = Calendar.getInstance().getTime();
					String fActual = actual.getDate() + "/"+(actual.getMonth()+1)+"/"+(actual.getYear()+1900);
					fecha = fecha.substring(0, 10);
					if(fecha.equals(fActual) && nombre.trim().equals(persona.trim()) && monto.contains("+ S/")){
						webElement.click();
						Element montoTotalApp = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]");
						monto = EFA.executeAction(Action.GetAttribute, montoTotalApp, "label").toString().replace("S/ ", "");
						if(Float.parseFloat(monto) == montoTotal && Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue())).toString())) {
							return true;
						}
						else{
							EFA.executeAction(Action.Click, btnEntendido);
						}						
						
					}
					else {
						if(!fecha.equals(fActual)){
							return false;
						}						
					}
				}
			}
			else {
				msjFinal = "No hay movimientos";
				return false;
			}
		}
		else {
			msjFinal = "No se muestra la pantalla de movimientos";
			return false;
		}
		
		return false;
	}
	
	public static boolean verificarDevolucion(float montoTotal, String persona) throws Exception {
		int tries = 0;
		Thread.sleep(2000);
		List<WebElement> filas = EFA.cv_driver.findElementsByXPath("/XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]");
		if(tries < 5 && filas.size() == 0) {
			Thread.sleep(2000);
			filas = EFA.cv_driver.findElementsByXPath("/XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[1]");
			tries++;
		}
		if(filas.size() > 0) {
			String nombre = filas.get(0).findElement(By.xpath(".//XCUIElementTypeStaticText[1]")).getAttribute("label");
			String fecha = filas.get(0).findElement(By.xpath(".//XCUIElementTypeStaticText[2]")).getAttribute("label");
			String monto = filas.get(0).findElement(By.xpath(".//XCUIElementTypeStaticText[3]")).getAttribute("label");
			Date actual = Calendar.getInstance().getTime();
			String fActual = actual.getDate() + "/"+(actual.getMonth()+1)+"/"+(actual.getYear()+1900);
			fecha = fecha.substring(0, 10);
			if(fecha.equals(fActual) && nombre.trim().equals(persona.trim()) && monto.contains("- S/")){
				filas.get(0).click();
				Element montoTotalApp = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]");
				monto = EFA.executeAction(Action.GetAttribute, montoTotalApp, "label").toString().replace("S/ ", "");
				Element btnEntendido = new Element("xpath", "//XCUIElementTypeButton[@name='backSpec']");
				if(Float.parseFloat(monto) == montoTotal) {
					EFA.executeAction(Action.Click, btnEntendido);
					return true;
				}
				
				EFA.executeAction(Action.Click, btnEntendido);
				
			}
		}
		else
		{
			msjFinal = "No se muestra ningun movimiento";
		}
		
		msjFinal = "No se realizo la devolucion.";
		return false;
	}
	
	public static boolean buscarCobroPasado(float montoTotal, String persona) throws Exception {
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue())).toString())){
			int tries = 0;
			Thread.sleep(2000);
			List<WebElement> filas = EFA.cv_driver.findElementsByXPath("/XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell");
			
			if(tries < 5 && filas.size() == 0) {
				Thread.sleep(2000);
				filas = EFA.cv_driver.findElementsByXPath("/XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell");
				tries++;
			}
			Element btnEntendido = new Element("xpath", "//XCUIElementTypeButton[@name='backSpec']");
			if(filas.size() > 0) {
				for (WebElement webElement : filas) {
					boolean visibleCelda = Boolean.parseBoolean(webElement.getAttribute("visible"));
					while(!visibleCelda){
						hacerSwipe();
						visibleCelda = Boolean.parseBoolean(webElement.getAttribute("visible"));
					}
					
					String nombre = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[1]")).getAttribute("label");
					String fecha = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[2]")).getAttribute("label");
					String monto = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[3]")).getAttribute("label");
					if(fecha.contains(" S/ ")){
						nombre = fecha;
						fecha = monto;
						
					}
					Date actual = Calendar.getInstance().getTime();
					String fActual = actual.getDate() + "/"+(actual.getMonth()+1)+"/"+(actual.getYear()+1900);
					fecha = fecha.substring(0, 10);
					if(!fecha.equals(fActual) && nombre.trim().equals(persona.trim()) && monto.contains("+ S/")){
						webElement.click();
						Element montoTotalApp = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]");
						monto = EFA.executeAction(Action.GetAttribute, montoTotalApp, "label").toString().replace("S/ ", "");
						if(Float.parseFloat(monto) == montoTotal && Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnEntendido).toString())) {
							return true;
						}						
						
					}
					else {
						if(!fecha.equals(fActual)){
							return false;
						}						
					}
				}
			}
			else {
				msjFinal = "No hay movimientos";
				return false;
			}
		}
		else {
			msjFinal = "No se muestra la pantalla de movimientos";
			return false;
		}
		
		return false;
	}
	
	public static void hacerSwipe() throws Exception {
		Dimension dimensions = EFA.cv_driver.manage().window().getSize();
		double dblStart,dblEnd;
		dblStart=0.9;
		dblEnd=0.3;
		Double screenHeightStart = dimensions.getHeight() *dblStart;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * dblEnd;
		int scrollEnd = screenHeightEnd.intValue();
		Double tempStart = scrollStart*dblStart;
		Double tempEnd = scrollEnd*dblEnd;
		String[] Arreglo = new String[] {"0",tempStart.intValue()+"","0",tempEnd.intValue()+"","2200"};
		//
		EFA.executeAction(Action.Swipe, Arreglo);
	}
}
