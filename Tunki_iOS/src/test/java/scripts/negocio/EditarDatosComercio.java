package scripts.negocio;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.UtilitariosErrores;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class EditarDatosComercio {
	private static String msjFinal = "";

	public static void editarComercio() throws Exception {

		Login.login();
		msjFinal = Login.getResult();
		if (!msjFinal.equals(Mensajes.SUCCESS))
			return;
		msjFinal = "";

		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if (!Boolean
				.parseBoolean(EFA
						.executeAction(Action.IsElementPresent,
								Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()))
						.toString())) {
			msjFinal = "Error: No se muestra lista de clientes";
			Thread.sleep(500);
			Reutilizable.tomarCapturaPantalla(300, "No se muestra los clientes");
			return;
		}

		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Menu Mas", 0);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_EDIT.getValue()));
		if (GlobalData.getData("viModificarFoto").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_GALLERY.getValue()));
			Reutilizable.galleryPhoto();
			EFA.cs_getTestEvidence("Cambio foto", 500);
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue())).toString())){
				msjFinal = "No muestra pantalla editar comercio";
				return;
			}
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			msjFinal = UtilitariosErrores.validarErrorTextoComercio();
			if (!msjFinal.isEmpty()) {
				return;
			}
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SIGNOUT.getValue())).toString())){
				msjFinal = "No muestra menu mas";
				return;
			}
			Reutilizable.tomarCapturaPantalla(300, "MenuMas");
		}
		if (GlobalData.getData("viModificarLocal").equals("SI")) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_STORE_NAME.getValue()));
			EFA.executeAction(Action.Clear,
					Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_STORE_NAME.getValue()));
			if(!GlobalData.getData("viNombreLocal").isEmpty()){
				EFA.executeAction(Action.SendKeys,
						Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.ET_STORE_NAME.getValue()),
						GlobalData.getData("viNombreLocal"));
			}
			Reutilizable.hacerTap(183, 237);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			msjFinal = UtilitariosErrores.validarErrorTextoComercio();
			if (!msjFinal.isEmpty()) {
				return;
			}
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SIGNOUT.getValue())).toString())){
				msjFinal = "No muestra menu mas";
				return;
			}
			Reutilizable.tomarCapturaPantalla(300, "MenuMas");
		}
	}

	public static String getResult() throws Exception {
		if (!GlobalData.getData("viMensajeErrorEditar").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorEditar").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}

		return msjFinal.isEmpty() ? Mensajes.SUCCESS : msjFinal;
	}

}
