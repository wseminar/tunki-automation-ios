package scripts.negocio;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.helpers.Mensajes;

public class Historial {
	private static String msjFinal = "";
	
	private static Element progress = new Element("xpath", "//*[@type='XCUIElementTypeActivityIndicator']");
	
	private static Element tituloHistorial = new Element("xpath", "//XCUIElementTypeStaticText[@label='Historial de movimientos']");
	
	public static void historial() throws Exception {		
		msjFinal = "";
		
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
		Element itemMovimientos = new Element("xpath", "//XCUIElementTypeButton[@label='Movimientos']");
			
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, itemMovimientos).toString())){
			msjFinal = "No se muestra pantalla de inicio";
			return;
			
		}		
		EFA.executeAction(Action.Click, itemMovimientos);		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, progress).toString()));	
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, tituloHistorial).toString())){
			Element lbltotaxdia = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeOther/XCUIElementTypeStaticText[1]");
			String totalDia = EFA.executeAction(Action.GetAttribute, lbltotaxdia, "label").toString();
			totalDia = totalDia.split(" S/ ")[1];
			float totalxdia = Float.parseFloat(totalDia.trim());
			//aqui
			
			if(GlobalData.getData("viValidarMovimientos").equals("SI")) {
				if(!validarMovimientos(totalxdia)) return;
			}
			
			
			if(GlobalData.getData("viEnviarReporte").equals("SI")){
				if (!enviarReporte()){
					return;
				}
			}	
		}
	}

	public static String getResult() throws Exception {
		
		if(!GlobalData.getData("viMensajeErrorHistorial").equals("")) {
			if(GlobalData.getData("viMensajeErrorHistorial").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje esperado no es igual: " + msjFinal;
		}
		
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
		
	}
	
	public static boolean enviarReporte() throws Exception{

		Element btnEnviarCorreo = new Element("xpath", "//XCUIElementTypeButton[@label='sendEmialAlert']");
		Element txtEmail = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField");
		Element lblErrorEnvioEmail = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeStaticText");
		Element btnEnviarEmail = new Element("xpath", "//XCUIElementTypeButton[@label='Enviar']");
		Element btnCancelarEmail = new Element("xpath", "//XCUIElementTypeButton[@label='Cancelar']");
		Element btnEntendido = new Element("xpath", "//XCUIElementTypeButton[@label='Entendido']");
		Element imgSendAlert = new Element("xpath", "//XCUIElementTypeImage[@name='sendAlert']");
		Element imgUpsAlert = new Element("xpath", "//XCUIElementTypeImage[@name='upsAlert']");
		Element lblUpsError = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeStaticText");
		
		EFA.executeAction(Action.Click, btnEnviarCorreo);
		
		EFA.executeAction(Action.Click, txtEmail);
		EFA.executeAction(Action.SendKeys, txtEmail, GlobalData.getData("viMail"));
		
		if(GlobalData.getData("viCancelarEnvio").equals("SI")) {
			EFA.executeAction(Action.Click, btnCancelarEmail);
			
			Thread.sleep(500);
			EFA.cs_getTestEvidence("Movimientos", 3);
			
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnCancelarEmail).toString())) {
				msjFinal = "No se muestra el historial después de cancelar el envio del reporte";
				return false;
			}
			else
				return true;
		}

		EFA.executeAction(Action.Click, btnEnviarEmail);	
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblErrorEnvioEmail).toString())) {
			Object mensajeErrorObj = EFA.executeAction(Action.GetAttribute, lblErrorEnvioEmail, "label");
			
			if(mensajeErrorObj != null){
				msjFinal = mensajeErrorObj.toString();
				EFA.executeAction(Action.Click, btnCancelarEmail);
				return false;
			}
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, progress).toString()));
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgSendAlert).toString())) {
			//msjFinal = "No se muestra el historial después de cancelar el envio del reporte";
			EFA.executeAction(Action.Click, btnEntendido);
			return true;
		}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgUpsAlert).toString())) {
			msjFinal = EFA.executeAction(Action.GetAttribute, lblUpsError, "label").toString();
			EFA.executeAction(Action.Click, btnEntendido);
			return false;
		}
		
		return false;
	}
	
	public static boolean validarMovimientos(float montoTotal) throws Exception {
		boolean bandera = false;
		float montoAcumulado = 0;
		List<WebElement> filas = EFA.cv_driver.findElementsByXPath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell");
		Element btnEntendido = new Element("xpath", "//XCUIElementTypeButton[@name='backSpec']");
		if(filas.size() > 0) {
			for (WebElement webElement : filas) {
				boolean visibleCelda = Boolean.parseBoolean(webElement.getAttribute("visible"));
				while(!visibleCelda){
					hacerSwipe();
					visibleCelda = Boolean.parseBoolean(webElement.getAttribute("visible"));
				}
				
				String signo = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[3]")).getAttribute("label");

				if(!signo.contains(" S/ ")){
					signo = webElement.findElement(By.xpath(".//XCUIElementTypeStaticText[2]")).getAttribute("label");
				}
				
				webElement.click();
				
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Detalle Movimiento", 1);
				
				String fecha = EFA.cv_driver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText[2]")).getAttribute("label");
				String monto = EFA.cv_driver.findElement(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]")).getAttribute("label");

				EFA.executeAction(Action.Click, btnEntendido);
				
				Thread.sleep(500);				
				EFA.cs_getTestEvidence("Movimientos", 2);
				
				Date actual = Calendar.getInstance().getTime();
				@SuppressWarnings("deprecation")
				String fActual = actual.getDate() + "/"+(actual.getMonth()+1)+"/"+(actual.getYear()+1900);
				fecha = fecha.substring(0, 10);
				if(fecha.equals(fActual)){
					
					EFA.cs_getTestEvidence("Visualizar Movimiento", 0);
					
					bandera = true;
					
					if(signo.contains("+")){
						montoAcumulado += Float.parseFloat(monto.replace("S/ ", "").trim());
					}
					else {
						montoAcumulado -= Float.parseFloat(monto.replace("S/ ", "").trim());
					}
					
				}
				else break;
			}
			if(!bandera) {
				msjFinal = "No hay movimientos para el día de hoy";
			}
			else
				if(GlobalData.getData("viValidarMontos").equals("SI")) {
					montoAcumulado = (float)Math.round(montoAcumulado * 100f) / 100f;
					if(montoAcumulado != montoTotal)
					{
						msjFinal = "Los montos no coinciden";
						bandera = false;
					}
				}
		}
		else {
			msjFinal = "No hay movimientos";
			bandera = false;
		}
		
		return bandera;
	}
	
	public static void hacerSwipe() throws Exception {
		Dimension dimensions = EFA.cv_driver.manage().window().getSize();
		double dblStart,dblEnd;
		dblStart=0.9;
		dblEnd=0.3;
		Double screenHeightStart = dimensions.getHeight() *dblStart;
		int scrollStart = screenHeightStart.intValue();
		Double screenHeightEnd = dimensions.getHeight() * dblEnd;
		int scrollEnd = screenHeightEnd.intValue();
		Double tempStart = scrollStart*dblStart;
		Double tempEnd = scrollEnd*dblEnd;
		String[] Arreglo = new String[] {"0",tempStart.intValue()+"","0",tempEnd.intValue()+"","2200"};
		//
		EFA.executeAction(Action.Swipe, Arreglo);
	}

	

}
