package scripts.negocio;

import com.everis.*;

import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class Cancelacion {

	private static String msjFinal = "";

	public static void cancelar() throws Exception{
		msjFinal = "";
		
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		
		msjFinal="";
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		if(!Cobro.buscarClientes(GlobalData.getData("viClienteNombre"), true)){
			msjFinal = "Error: No se encontró el cliente.";
			return;
		}
		
		if(!Cobro.EnvioCobro()) {
			msjFinal = "No se realizo el cobro correctamente";
			return;
		}
		
		if(GlobalData.getData("viCancelarCobro").equals("SI")){
			
			int nVecesCancelar = Integer.parseInt(GlobalData.getData("viVecesCancelar"));
			
			for (int i = 0; i < nVecesCancelar; i++) {
				
				if(!GlobalData.getData("viTiempoEspera").isEmpty()) {
					int segundosEspera = Integer.parseInt(GlobalData.getData("viTiempoEspera"));
					Thread.sleep(segundosEspera * 1000); 
				}
				
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CANCEL.getValue()));			
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));	
				Reutilizable.tomarCapturaPantalla(500, "Cobro Cancelado");
								
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
				
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					
				if((i+1) == nVecesCancelar) {
					if(GlobalData.getData("viContinuarFlujo").equals("SI")) {
						if(!Cobro.buscarClientes(GlobalData.getData("viClienteNombre"), true)){
							msjFinal = "Error: No se encontró el cliente.";
							return;
						}
						
						if(!Cobro.EnvioCobro()) {
							msjFinal = "No se realizo el cobro correctamente";
							return;
						}
					}
					else {
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
						
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
						
						Element lblHistorial = new Element("xpath", "//XCUIElementTypeNavigationBar[@name='Historial de movimientos']");
						if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, lblHistorial).toString())) {
							Reutilizable.tomarCapturaPantalla(500, "Movimientos");
						}
						else {
							Reutilizable.tomarCapturaPantalla(500, "Error Movimientos");
							msjFinal = "Error al entrar al historial";
						}
						return;
					}
				}
				else {
					if(!Cobro.buscarClientes(GlobalData.getData("viClienteNombre"), true)){
						msjFinal = "Error: No se encontró el cliente.";
						return;
					}
					
					if(!Cobro.EnvioCobro()) {
						msjFinal = "No se realizo el cobro correctamente";
						return;
					}
				}
				
			}
		}
	
		Element sgEsperarConfirmacionCliente = new Element("xpath", "//XCUIElementTypeImage[@name='iconCheckinSpec']");
		int tries = 0;
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Espera Confirmacion", 0);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, sgEsperarConfirmacionCliente).toString())) {
			if(tries == 60) {
				msjFinal = "La persona no acepto la solicitud de cobro.";
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CANCEL.getValue()));
				return;
			}
			tries++;
			Thread.sleep(5000);
		}
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_PAYMENT_ID.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetAttribute,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_TITTLE.getValue()), "label").toString();
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			return;
		}
		
		EFA.cs_getTestEvidence("Confirmacion", 0);
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
				
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		Thread.sleep(500);
		
		EFA.cs_getTestEvidence("Historial", 0);
		
		 //SE COMENTO
		/*if(!Cobro.validarClientesHistorial(GlobalData.getData("viClienteNombre") + " " + GlobalData.getData("viClienteApellido"))){
			msjFinal = "Error: No se visualiza el último movimiento realizado";
			return;
		}*/
	}
	
	public static String getResult() throws Exception{
		
		return msjFinal.isEmpty()?Mensajes.SUCCESS:msjFinal;
	}
	
}
