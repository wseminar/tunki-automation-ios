package scripts.negocio;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class SesionActiva {
	private static String msjFinal = "";
	private static String sgsesionActiva="";

	public static void sesionActiva() throws Exception{
		
		Login.login();
		msjFinal = Login.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		
		//INICIO NUEVO FLUJO
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));
     
		//Validacion Lista Clientes
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue())).toString())) {
			msjFinal = "Error: No se muestra lista de clientes";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SETTING.getValue()));	
		sgsesionActiva = EFA.executeAction(Action.GetAttribute,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SW_SESSION_ALIVE.getValue()),"value").toString();
		sgsesionActiva = sgsesionActiva.equals("1")?"SI":"NO";
		String indicadorSesion = "NO";
		if(GlobalData.getData("viIsMantenerSesion").equals("SI") || GlobalData.getData("viMantenerSesion").equals("SI"))
			indicadorSesion = "SI";
		
		if(!sgsesionActiva.equals(indicadorSesion)){
			msjFinal = "El indicador de sesión activa no tiene el mismo valor que en la base de datos";
			return;
		}
		
		if(GlobalData.getData("viCambiarEstado").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SW_SESSION_ALIVE.getValue()));
			sgsesionActiva = EFA.executeAction(Action.GetAttribute,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.SW_SESSION_ALIVE.getValue()),"value").toString();
			sgsesionActiva = sgsesionActiva.equals("1")?"SI":"NO";
		}
		EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BACK_BLUE.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SIGNOUT.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));
		
		if(GlobalData.getData("viReIngreso").equals("SI")) {
			EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()),GlobalData.getData("viUsuario"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
			if(GlobalData.getData("viReMantenerSesion").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHECK_REMEMBER.getValue()));
			}
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()),GlobalData.getData("viPassword"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty()) return;
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_NOW.getValue())).toString())){
				Reutilizable.tomarCapturaPantalla(0, "Pop UP");
			}
			return;
		}
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getValue())).toString())){
			msjFinal = "Error: No se muestra panel de Login";
			return;
		}
		
	}
	
	
	public static String getResult() throws Exception{
		if(msjFinal.isEmpty())
			return Mensajes.SUCCESS;
		return msjFinal;
		
	}
}
