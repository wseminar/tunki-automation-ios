package scripts.negocio;

import java.util.Date;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class Login {
	private static String msjFinal = ""; 
	
	public static void login() throws Exception{
		
		msjFinal = "";
		
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue())).toString()));
	
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()), GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()), GlobalData.getData("viPassword"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
		
		if(GlobalData.getData("viMantenerSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHECK_REMEMBER.getValue()));
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getValue()));	
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		
		Thread.sleep(2000);
		EFA.cs_getTestEvidence("Pantalla Login", 0);
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_NOW.getValue())).toString()))
		{
			if(GlobalData.getData("viIsMantenerSesion").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			}else {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NOT_NOW.getValue()));
			}
		}
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Correcto Login", 0);

	}
	
	public static String getResult() throws Exception{
		
		if(!GlobalData.getData("viMensajeError").isEmpty()){
			if(GlobalData.getData("viMensajeError").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
				
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
		
	}
	
	private static Boolean validarSesionBD(){
		String[] param = new String[1];
		Date caseTime = new Date();
	    Date caseTimeCurrent = new Date();
		try {
			caseTime.setTime(System.currentTimeMillis());
			caseTimeCurrent.setTime(System.currentTimeMillis());
			String token = ValidacionBD.negocio_ValidarToken(GlobalData.getData("viUsuario"));
			if(token.equals(Mensajes.DATANOTFOUND)) 
			{
				//msjFinal = "El token es nulo";
				EFA.cs_efaLogTest(0, "Token: 'El token es nulo'", "Base de datos", param, EFA.cf_getDiffTime(caseTime, caseTimeCurrent), "type");
				return false;
			}
			GlobalData.setData("vOutTokenDevice", token);
			param[0] = token;
			EFA.cs_efaLogTest(0, "Token: '"+token+"'", "Base de datos", param, EFA.cf_getDiffTime(caseTime, caseTimeCurrent), "type");
			System.out.println(token);
		} catch (Exception e) {
			//msjFinal = "Error en la conexión a la base de datos";
			GlobalData.setData("vOutTokenDevice", e.getMessage());
			EFA.cs_efaLogTest(0, "Error Token: '"+e.getMessage()+"'", "Base de datos", null, EFA.cf_getDiffTime(caseTime, caseTimeCurrent), "type");
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	public static void loginRegistro(Element validacion) throws Exception {
		msjFinal = "";
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue())).toString()));
		
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_USER_NAME.getValue()), GlobalData.getData("viUsuario"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD.getValue()), GlobalData.getData("viPassword"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_LOGO.getValue()));
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SIGN_IN.getValue()));	
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, validacion).toString())) {
			msjFinal = "No se visualiza la pantalla verde";
		}
		
		EFA.cs_getTestEvidence("Pantalla", 0);
		
	}
}
