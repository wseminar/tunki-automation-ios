package scripts.identifiers;

public enum ETextArea {
	ET_EMAIL("et_email"),
	ET_PASSWORD("et_password"),
	TXT_MESSAGE("txt_message"),
	ET_DOCUMENT("et_document"),
	ET_NAME("et_name"),
	ET_LAST_NAME("et_last_name"), 
	ET_CODE_1("et_code_1"),
	TXT_DOCUMENT_NUMBER("txt_document_number"),
	ET_CARD_NUMBER("et_card_number"),
	ET_DOC_NUMBER("et_doc_number"),
	ET_CVV("et_cvv"), 
	ET_EXPIRATION_DATE("et_expiration_date"), 
	ET_PASSWORD_BPI("et_password_bpi"),
	ET_TIP("et_tip"),
	TV_TITTLE("tv_title"),
	ET_AMOUNT("lbl_amount"),
	ET_USER_NAME("et_user_name"),
	ET_PASSWORD_CONFIRMATION("et_password_confirmation"),
	ET_COMMERCE("et_commerce"),
	ET_BUSINESS_NAME("et_business_name"),
	ET_RUC_NUMBER("et_ruc_number"),
	ET_ADMIN_NAME("et_admin_name"),
	ET_ADMIN_LAST_NAME("et_admin_last_name"),
	ET_ADMIN_EMAIL("et_admin_email"),
	ET_ADMIN_PHONE("et_admin_phone"),
	ET_ADMIN_DOC_NUMBER("et_admin_doc_number"),
	ET_CONFIRM_PASSWORD("et_confirm_password"),
	TV_PAYMENT_ID("tv_payment_id"),
	ET_STORE_NAME("et_store_name"),
	ET_STORE_ADDRESS("et_store_address"),
	ET_ACCOUNT_NUMBER("et_account_number"),
	TXT_EMAIL("txt_email"),
	TXT_PASSWORD("txt_password"),
	TXT_CONFIRM_PASSWORD("txt_confirmPassword"),
	TXT_CONFIRM_PASSWORD_2("txt_confirm_password"),
	TXT_CODE("txt_code"),
	TXT_NAME("txt_name"),
	TXT_LASTNAME("txt_lastName"),
	TXT_DOCUMENT("txt_document"),
	TXT_PHONE("txt_phone"),
	TXT_DEBITCARD("txt_debitcard"),
	TXT_CREDITCARD("txt_Creditcard"),
	TXT_CVV("txt_cvv"),
	TXT_DATE_EXPIRED("txt_dateExpired"),
	TXT_P2P_NAME("txt_p2p_name"),
	TXT_DETAIL("txt_detail"),
	TXT_NUMBER("txt_number"),
	LBL_NAME("lbl_name"),
	TXT_REPEAT_PASSWORD("txt_repeat_password"),
	LBL_TITTLE("lbl_title"),
	TV_STORE_NAME("tv_store_name"),
	TXT_AMOUNT("txt_amount"),
	TXT_QUANTITY("txt_quantity");

	private String value;
	
	ETextArea(String value){
		this.value = value;
	} 
	
	public String getValue() {
		return value;
	}
}
