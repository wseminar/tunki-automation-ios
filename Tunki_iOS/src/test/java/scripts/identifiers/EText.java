package scripts.identifiers;

public enum EText {
	TV_DETAIL("tv_detail"),
	LBL_TITLE("lbl_title"), 
	BOTTOM_TEXT("bottom_text"),
	TV_CODE("tv_code"),
	VALIDA_TU_CORREO("Valida tu correo"),
	RESTABLECE_TU_CONTRASEÑA("//XCUIElementTypeStaticText[@name='Restablece tu contraseña']"),
	DETALLE_RESTABLECE_TU_CONTRASEÑA("Tu cuenta ha sido temporalmente bloqueada por intentos fallidos. Para acceder a Tunki debes crear una nueva contraseña."),
	PASS_DIFERENTE("No coinciden las contraseñas"),
	CREDENCIALES_INVALIDAS("Credenciales inválidas."),
	TV_EMPTY("Por el momento no tienes tickets"),
	DESCRIPCION_ERROR_TICKETS("Has comprado el máximo de tickets permitidos por periodo."),
	//Textos para comparar en el item
	ITEM_VINCULAR_TARJETA("//XCUIElementTypeStaticText[@name='Vincula tu tarjeta']"),
	ITEM_MIS_TICKETS("//XCUIElementTypeStaticText[@name='Mis Tickets']"),
	ITEM_BENEFICIOS("//XCUIElementTypeStaticText[@name='Beneficios Tunki']"),
	ITEM_CONFIGURACION("//XCUIElementTypeStaticText[@name='Configuración']"),
	ITEM_SUGERIR_COMERCIO("//XCUIElementTypeStaticText[@name='Sugerir Comercios']"),
	ITEM_CONTACTANOS("//XCUIElementTypeStaticText[@name='Contáctanos']");
	
	
	private String value;
	
	EText(String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
