package scripts.identifiers;

public enum EBarButtons {
	
	COMERCE("menu_list_commerce"),
	MOVES("Movimientos"),
	MORE("Más"),
	MENU_LIST_P2P_CONTACTS("menu_list_p2p_contacts"),
	MENU_LIST_SERVICE("menu_list_service");
	
	private String value;
	
	EBarButtons(String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
