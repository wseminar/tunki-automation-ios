package scripts.identifiers;

public enum EButtons {
	
	BTN_SIGN_IN("btn_sign_in"),
	BTN_NOT_NOW("btn_not_now"),
	BTN_ACCEPT("btn_accept"),
	BTN_CREATE("btn_create"),
	BTN_TIPO_DOC("dniRow2"), 
	BTN_NEXT("btn_next"),
	BTN_CONTACTS("btnContacts"),
	HIDE_PASSWORD("hidePassword"),
	ITEM_SIGNOUT("item_signout"),
	BTN_CONFIRM("btn_confirm"),
	ITEM_SETTING("item_setting"),
	BTN_LINK_CARD("btn_link_card"),
	BTN_GO_TO_STORE_LIST("btn_go_to_store_list"),
	BTN_OPEN_ACCOUNT("btn_open_account"),
	BACK_BLUE("back blue"),
	ITEM_ACCOUNT_MANAGMENT("item_account_managment"),
	SW_SESSION_ALIVE("sw_session_alive"),
	TARJETA_CREDITO("Tarjeta de crédito"),
	TARJETA_DEBITO("Tarjeta de débito"),
	SPINNER_DOCUMENT("spinner_document"),
	BTN_OK("btn_ok"),
	EDITAR("Editar"),
	DELETEP_AYMENT_METHOD("deletePaymentMethod"),
	BTN_CANCEL("btn_cancel"),
	BTN_GENERATE_CODE("btn_generate_code"),
	BTN_CHANGE_COMMERCE("btn_change_commerce"),
	BTN_PAY("btn_Pay"),
	BTN_CHECK_REMEMBER("btn_check_remenber"),
	BTN_FORGOT_PASSWORD("btn_forgot_password"),
	BTN_CHECK_COND_TERMS_USE("btn_check_cond_terms_use"),
	MASUNLOCAL("Más de un local"),
	LBL_DOCUMENT("lbl_document"),
	BTN_TERMS_CONDITION("btn_terms_condition"),
	BTN_SEND_COLLECT("btn_send_collect"),
	BUSCADOR("Buscador"),
	TXT_CANCEL("txt_cancel"),
	TV_GO_TO_LOGIN("tv_go_to_login"),
	BTN_ADD_LOCAL("btn_add_local"),
	BTN_GALLERY("btn_gallery"),
	BTN_AGREED("btn_agreed"),
	BTN_SEND("btn_send"),
	BTN_DECLINE("btn_decline"),
	CUENTA_AHORROS("Cuenta Ahorros"),
	CUENTA_CORRIENTE("Cuenta Corriente"),
	BTN_START("btn_start"),
	BTN_REJECT("btn_reject"),
	BTN_LOGIN("btn_login"),
	BTN_REGISTER("¿Aún no estás registrado?"),
	BTN_GO_TO_LOGIN("btn_gotoLogin"),
	POR_AHORA_NO("Por ahora no"),
	ACEPTAR("Aceptar"),
	BTN_BUY("btn_buy"),
	REINTENTAR("Reintentar"),
	ATRAS("Atrás"),
	BTN_DOCUMENT_TYPE("btn_DocumentType"),
	PINLEFT("pinLeft"),
	NEXT("btn_Next"),
	ENTENDIDO("Entendido"),
	CONTINUAR_SIN_TARJETA("btn_open_account"),
	BTN_GO_TO_HOME("btn_gotoHome"),
	BTN_CAMERA("btn_Camera"),
	BTN_TAKE_PICTURE("Tomar foto"),
	BTN_OPEN_GALLERY("Seleccionar foto"),
	BTN_EDIT("btn_edit"),
	PHOTO_CAPTURE("PhotoCapture"),
	USE_PHOTO("Use Photo"),
	RETAKE("Retake"),
	DONE("Done"),
	IMG_PHOTO("img_photo"),
	BTN_COLLECT("btn_Collect"),
	BTN_TARJETA_IBK("btn_ibk"),
	BTN_RETRY("btn_retry"),
	BTN_FORGOTPASSWORD("btn_forgotPassword"),
	ENVIAR("Enviar"),
	CONFIRMAR("Confirmar"),
	CANCEL("Cancel"),
	EMPEZAR("empezar"),
	CONTINUAR("Continuar"),
	BTN_SHARE_INVOICE("btn_share_invoice"),
	BTN_VALIDAR_AHORA("Validar ahora"),
	BTN_VALIDAR_DESPUES("Validar después"),
	VALIDAR("Validar"),
	BTN_EMPEZAR("Empezar"),
	PAGAR("Pagar"),
	RECHAZAR("Rechazar"),
	CELL_RECHARGE("cell_recharge"),
	BTN_CAMBIAR_CUENTA("Cambiar de cuenta"),
	BNN_0("btn_0"),
	RESTABLECER("Restablecer"),
	RECOVER_PASSWORD("Restablecer contraseña"),
	CE("CE"),
	PASAPORTE("Pasaporte"),
	BTN_RESEND("Reenviar código"),
	CANCELAR("Cancelar"),
	BTN_0("btn_0"),
	TV_EDIT("Editar"),
	TAKESELFIESPEC("takeSelfieSpec"),
	UPLOADPHOTOSPEC("uploadPhotoSpec"),
	BTN_USENOW("btn_usenow"),
	BTN_USELATER("btn_uselater"),
	ITEM_TICKETS("item_tickets"),
	BTN_ADD("btn_add"),
	IMAGE_CODE("image_code"),
	VERIFICAR("Verificar"),//
	ITEM_BENEFICIOS("item_promotions"),
	BTN_TERM_CONDITION("btn_term_condition"),
	BTN_TUTORIAL("btn_tutorial"),
	ITEM_RECOMMEND_COMMERCE("item_recommend_commerce"),
	ITEM_CONTACTANOS("item_contactus"),
	BTN_ACEPTAR_TARJETA("(//XCUIElementTypeButton[@name='Aceptar'])[2]");

	private String value;
	
	EButtons(String value){
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
}
