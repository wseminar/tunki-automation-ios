package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import scripts.helpers.*;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;

public class Cancelacion {

	private static String msjFinal = "";
	
	public static void cancelar() throws Exception{
		msjFinal = "";
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		if(!Pago.buscarComercio(GlobalData.getData("viComercio"))){
			msjFinal = "No se encontró el comercio.";
			return;
		}
	
		int tries = 0;
		if(GlobalData.getData("viComercioRechaza").equals("SI")) {
			
			int nVecesCancelar = Integer.parseInt(GlobalData.getData("viVecesCancelar"));
			
			for (int i = 0; i < nVecesCancelar; i++) {
			
				if(!GlobalData.getData("viTiempoEspera").isEmpty()) {
					int segundosEspera = Integer.parseInt(GlobalData.getData("viTiempoEspera"));
					Thread.sleep(segundosEspera * 1000); 
				}
				
				tries = 0;
				
				while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMAGE_VIEW.getValue())).toString())) {
					
					if(tries == 60)
					{
						msjFinal = "El comercio no cancelo el cobro.";
						return;
					}
					Thread.sleep(5000);
					tries++;
				}
				
				EFA.cs_getTestEvidence("Cobro Cancelado", 0);
				
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
				
				tries = 0;
				
				if((i+1) == nVecesCancelar) {
					if(GlobalData.getData("viContinuarFlujo").equals("SI")) {
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WAITING.getValue())).toString())) {
							
							if(tries == 60)
							{
								msjFinal = "No se recibió ninguna solicitud de cobro.";
								return;
							}
							Thread.sleep(5000);
							tries++;
						}
					}
					else {
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHANGE_COMMERCE.getValue()));
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
						if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue())).toString())) {
							Reutilizable.tomarCapturaPantalla(500, "Movimientos");
						}
						else {
							Reutilizable.tomarCapturaPantalla(500, "Error Movimientos");
							msjFinal = "Error al entrar al historial";
						}
						return;
					}
				}
				else {
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WAITING.getValue())).toString())) {
						
						if(tries == 60)
						{
							msjFinal = "No se recibió ninguna solicitud de cobro.";
							return;
						}
						Thread.sleep(5000);
						tries++;
					}
				}				
			}
		}
		
		if(!Pago.escribirPropina()) {
			msjFinal = Pago.getMensaje();
			return;
		}
		
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IMAGE_VIEW.getValue())).toString())){
			EFA.cs_getTestEvidence("Pantalla", 0);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			//if(GlobalData.getData("viValidarMovimiento").equals("SI")){
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				if(!Pago.validarComercioHistorial(GlobalData.getData("viComercio"))){
					msjFinal = "No se visualiza el último movimiento realizado";
				}
			//}
		}
		else
		{
			msjFinal = "El pago no se realizo correctamente";
			EFA.cs_getTestEvidence("Pantalla Error", 0);
			return;
		}
	}
	
	public static String getResult() throws Exception {
		
		return msjFinal.isEmpty()?Mensajes.SUCCESS:msjFinal;
	}
	
}
