package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePxP;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;

public class CobroChat{
	public static String msjFinal = "";
	public static void cobroChat() throws Exception {
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";  
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		x.tap(112, 641).perform();
		EFA.cs_getTestEvidence("Lista de Contactos", 500);
		ReusablePxP.buscarPersona(GlobalData.getData("viPersonaCobrar"));
		EFA.cs_getTestEvidence("Chat", 500);
		msjFinal = ReusablePxP.envioCobroP2P(GlobalData.getData("viMontoCobro"));
		if(!msjFinal.isEmpty()) return;
		Thread.sleep(5000);
		if(!ReusablePxP.capturarRespuestaChat(Textos.ENVIASTE_UN_COBRO,"S/ "+GlobalData.getData("viMontoCobro"))) {
			msjFinal = "no muestra ultimo cobro";
			return;
		}
		System.out.println("tiempo");
		Thread.sleep(20000);
		System.out.println("paso tiempo");
		if(!ReusablePxP.capturarRespuestaChat(Textos.RECIBISTE_UN_PAGO,"S/ "+GlobalData.getData("viMontoCobro"))) {
			msjFinal = "no muestra ultimo pago";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
		TouchAction z = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		z.press(91,375).moveTo(3,-136).release().perform();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.tomarCapturaPantalla(500, "Historial"); 
		if(GlobalData.getData("viValidarMovimiento").equals("SI")) {
			TouchAction s = new TouchAction((PerformsTouchActions)EFA.cv_driver);
			s.tap(155,91).perform();
			Reutilizable.tomarCapturaPantalla(300, "Constancia");	
		}
	}
	
	
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorCobro").isEmpty()){
			if(GlobalData.getData("viMensajeError").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
				
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
		
	}
}
