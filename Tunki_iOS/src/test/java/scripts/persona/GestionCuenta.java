package scripts.persona;

import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.*;

import script.textosTunki.Textos;
import net.bytebuddy.agent.builder.AgentBuilder.CircularityLock.Global;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePagoComercio;
import scripts.helpers.ReusableRegistroPersona;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class GestionCuenta {
  
	private static String msjFinal = "";
	private static String tarjeta = "";

	public static void gestionCuenta() throws Exception{
		if(!GlobalData.getData("viStatusPca").isEmpty()){
			String dni = ValidacionBD.obtenerDnixPersonId(GlobalData.getData("viEmail"));
			System.out.println(dni);
			ValidacionBD.cambiarDefaultAhorro(dni);
			ValidacionBD.cambiarDefaultMillonaria(dni);
		}
		if(GlobalData.getData("viStatusPca").toUpperCase().equals("QUITAR VALIDACION")){
			ValidacionBD.cambiarStatusValidarPca(GlobalData.getData("viEmail"),0);
		}
		if(GlobalData.getData("viStatusPca").toUpperCase().equals("INSERTAR VALIDACION")){
			ValidacionBD.cambiarStatusValidarPca(GlobalData.getData("viEmail"),1);
		}
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			String dato = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			System.out.println(dato);
			if(!dato.equals("DataNotFound")){
				ValidacionBD.eliminarTarjetaTblPMethod(dato);
				ValidacionBD.eliminarTarjetaTblPUser(dato);
				System.out.println("se borro tarjeta");
			}
		}
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("TARJETA MILLONARIA")){
			String dni = ValidacionBD.obtenerDnixPersonId(GlobalData.getData("viEmail"));
			System.out.println(dni);
			ValidacionBD.eliminarTarjeteaMillonaria(dni);
		}
		msjFinal = "";
		tarjeta = "";
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		if(GlobalData.getData("viP2C").equals("SI")) {
			if(!ReusablePagoComercio.buscarComercio(GlobalData.getData("viComercio"))){
				msjFinal = "no se encontro el comercio";
				return;
			}
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty())return;
			msjFinal = ReusablePagoComercio.pagoComercio();
			if(!msjFinal.isEmpty()) return;
		}
		msjFinal = Reutilizable.navegarMenuGestionCuentas();
		if(!msjFinal.isEmpty())return;
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.LBL_NAME.getValue())).toString())) {
			tarjeta = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_NAME.getValue())).toString().trim();
			System.out.println(tarjeta);
		}
		if(GlobalData.getData("viNavegacion").toUpperCase().equals("BACK")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
		}
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue())).toString())) {
				msjFinal = "No muestra pantalla Vincula tu Tarjeta de debito";
				return;
			}
			msjFinal = ReusableRegistroPersona.AgregarTarjeta();
			if(!msjFinal.isEmpty()) return;
			if(GlobalData.getData("viTipoTarjeta").toUpperCase().equals("DEBITO")) {
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue())).toString())) {
					if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("VARIAS CUENTAS")) {
						CapturarTextoPorId(tarjeta);
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
						while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
						msjFinal = Reutilizable.validarErrores();
						if(msjFinal.trim().toUpperCase().equals(Textos.TARJETA_YA_ESTA_VINCULADA.toUpperCase())) {
							EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
							msjFinal = ReusableRegistroPersona.AgregarTarjeta();
							if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue())).toString())) {
								CapturarTexto(tarjeta);
								EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
								while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
								msjFinal = Reutilizable.validarErrores();
								if(!msjFinal.isEmpty())return;
								EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
								while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
								msjFinal = Reutilizable.validarErrores();
								if(!msjFinal.isEmpty())return;
									if(GlobalData.getData("viPagoP2P").equals("SI")) {
										if(!ReusableRegistroPersona.pagoP2P())return;
									}	
							}
						}
					return;
					}
					if(!msjFinal.isEmpty()) return;
				}	
			if(msjFinal.trim().toUpperCase().equals(Textos.TARJETA_YA_ESTA_VINCULADA.trim().toUpperCase())) {
				if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("CUENTA UNICA")) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
					if(GlobalData.getData("viPagoP2P").equals("SI")) {
						if(!ReusableRegistroPersona.pagoP2P())return;
					}
					return;
				}
			}
			if(!msjFinal.isEmpty()) return;
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty())return;
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_DESPUES.getValue())).toString())) {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_DESPUES.getValue()));
			}
			EFA.cs_getTestEvidence("Menu mas", 1500);
				if(GlobalData.getData("viPagoP2P").equals("SI")) {
					if(!ReusableRegistroPersona.pagoP2P())return;
				}	
			}
			
	}
	
	public static boolean CapturarTextoPorId(String Texto) throws Exception {
		for(int i=1;i<3;i++) {
			String error = "";
			System.out.println(Texto);
			List<WebElement> lstMovimientosChat = EFA.cv_driver.findElementsByXPath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell["+i+"]");
			                                                                         //XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell[1]
				for(WebElement webElement : lstMovimientosChat){
					WebElement btnRadio = webElement.findElement(By.name("checkLoginUnSelected"));
					try {
						boolean xTarjeta = webElement.findElement(By.id(Texto)).isEnabled();
					} catch (Exception e) {
						error = "no encontro elemento";
						continue;
					}
					if(error.isEmpty()) {
						WebElement tarjeta = webElement.findElement(By.id(Texto));
						String tempTexto = tarjeta.getText().trim().toUpperCase();
						System.out.println(tempTexto);
						if(tempTexto.equals(Texto.toUpperCase())) {
							btnRadio.click();
							return true;
						}
					}
				}
		}
		return false;
	}
	
	public static boolean CapturarTexto(String Texto) {
		for(int i=1;i<3;i++) {
			String error = "";
			List<WebElement> lstMovimientosChat = EFA.cv_driver.findElementsByXPath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable/XCUIElementTypeCell["+i+"]");
				for(WebElement webElement : lstMovimientosChat){
					WebElement btnRadio = webElement.findElement(By.name("checkLoginUnSelected"));
					try {
						boolean xTarjeta = webElement.findElement(By.id(Texto)).isEnabled();
					} catch (Exception e) {
						error = "no encontro elemento";
					}
					if(!error.isEmpty()) {
							btnRadio.click();
							return true;
					}
				}
		}
		return false;
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorGestionCuenta").isEmpty()) {
			if(GlobalData.getData("viMensajeErrorGestionCuenta").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		
		return msjFinal.isEmpty() ? Mensajes.SUCCESS:msjFinal;
	}
	
}
