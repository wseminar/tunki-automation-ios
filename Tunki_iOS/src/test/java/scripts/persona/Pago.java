package scripts.persona;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import script.textosTunki.Textos;
import scripts.Util.UtilPaymentCommerce;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
 
public class Pago { 

	private static String msjFinal = "";
	private static Element btnPorcentajePropina = null;
	private static Element imgRocket = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther");

	public static void pagar() throws Exception{
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			String dato = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			System.out.println(dato);
			if(!dato.equals("DataNotFound")){
				ValidacionBD.eliminarTarjetaTblPMethod(dato);
				ValidacionBD.eliminarTarjetaTblPUser(dato);
				System.out.println("se borro tarjeta");
			}
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("SI")){
			String personId = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			System.out.println(personId);
			String paymentMethodId = ValidacionBD.obtenerPaymentMethodId(personId);
			System.out.println(paymentMethodId);
			String status = ValidacionBD.obtenerStatusTblPca(paymentMethodId);
			System.out.println(status);
			if(paymentMethodId.equals("DataNotFound") || !status.equals("0")){
				ValidacionBD.cambiarStatusTblPcaTD(paymentMethodId);
			}
		}
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		if(!msjFinal.isEmpty()) return;
		Thread.sleep(8000);
		if(!buscarComercio(GlobalData.getData("viComercio"))){
			msjFinal = "No se encontró el comercio.";
			return;
		}
		if(GlobalData.getData("viPagoSinTarjeta").toUpperCase().equals("SI")) {
			msjFinal = UtilPaymentCommerce.payNoCard();
			if(!msjFinal.isEmpty())return;	
		}
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_STORE_NAME.getValue())).toString())){
			msjFinal = "no muestra pantalla de cobro";
			return;
		}
		if(GlobalData.getData("viCheckOut").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHANGE_COMMERCE.getValue()));		
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Logout.logout(); 
			msjFinal = Logout.getResult();
			return;
		}
		
		if(GlobalData.getData("viSalirComercio").equals("SI")){
			while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_WAITING.getValue())).toString()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_REJECT.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			validarListaComercios();
			return;
		}	
		
		if(GlobalData.getData("viCambiarComercio").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CHANGE_COMMERCE.getValue()));
			validarListaComercios();
			return;
		}		
		int tries = 0;
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_STORE_NAME.getValue())).toString())) {
			if(tries==5)
			{
				msjFinal = "No se recibió ninguna solicitud de cobro.";
				return;
			}
			Thread.sleep(25000);
			tries++;
		}
		EFA.cs_getTestEvidence("Pantalla Pagar a", 800);
		if(GlobalData.getData("viRechazarPagoComercio").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RECHAZAR.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString())){
				 msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().trim();
				 return;
			}
		}
		Reutilizable.hacerTap(336,335);
		Thread.sleep(3000);
		EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()) {
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				UtilPaymentCommerce.EleccionTarjetaCredito();
				break;
			case "DEBITO":
				UtilPaymentCommerce.EleccionTarjetaDebito();
			default:
				break;
			}
		}else {
			Reutilizable.hacerTap(261,159);
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		}
		if(!escribirPropina()) return;	
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PAGAR.getValue()));
		if(GlobalData.getData("viValidarAhora").toUpperCase().equals("PAGO")) {
			EFA.cs_getTestEvidence("Pantalla Validacion", 1000);
			msjFinal = UtilPaymentCommerce.validateTarjeta();
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
				msjFinal = "";
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
				EFA.cs_getTestEvidence("pago", 1500);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PAGAR.getValue()));
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.BIRD.getValue())).toString()));
				if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().equals(Textos.PAGO_REALIZADO)){
					msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().trim();
					return;
				}
				Thread.sleep(10000);
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SHARE_INVOICE.getValue())).toString())) {
					msjFinal = "No se realizo la transferencia";
					return;
				}
				EFA.cs_getTestEvidence("Pago Realizado5", 1000);
				EFA.cs_getTestEvidence("Pago Realizado6", 1000);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			}
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.BIRD.getValue())).toString()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().trim();
			 return;
		}
		Reutilizable.tomarCapturaPantalla(800, "Pago Realizado");	
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
			Reutilizable.tomarCapturaPantalla(800, "Captura");
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Reutilizable.tomarCapturaPantalla(300, "Historial");
			if(GlobalData.getData("viValidarMovimiento").equals("SI")){
			TouchAction z = new TouchAction((PerformsTouchActions) EFA.cv_driver);
			z.tap(159,94).perform();
			Reutilizable.tomarCapturaPantalla(300, "Constancia");
		}
	}
	
	public static String getResult() throws Exception{
		String mensajeEsperado = GlobalData.getData("viMensajeErrorPago");
		if(!mensajeEsperado.equals("")){
			if(mensajeEsperado.equals(msjFinal)){
				return Mensajes.SUCCESS;
			}
			return "El mensaje no es igual: " + msjFinal;
		}
		
		if(msjFinal.equals(""))
			return Mensajes.SUCCESS;
		return msjFinal;
	}
	
	public static boolean buscarComercio(String comercio) throws Exception{
		EFA.cf_getTestEvidenceWithStep("LISTA DE COMERCIO", 500);
		EFA.cv_driver.findElementByXPath("(//XCUIElementTypeSearchField[@name='Buscar'])[1]").sendKeys(comercio.substring(0, comercio.length()-2));
		Thread.sleep(15000);
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.BUSCAR_COMERCIO.getValue()), comercio.substring(comercio.length()-2, comercio.length()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		List<WebElement> lista = EFA.cv_driver.findElements(By.id("txt_name"));
		for (WebElement item : lista) {
			if(item.getText().equals(comercio)){
				if(GlobalData.getData("viDetalleComercio").equals("SI")) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.IV_INFO.getValue()));
					Reutilizable.tomarCapturaPantalla(300, "Detalle Comercio");
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_PAY.getValue()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					Thread.sleep(500);
					EFA.cs_getTestEvidence("Espera Cobro", 0);
					return true;
				}
				item.click();
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Espera Cobro", 0);
				return true;
			}
		}
		return false;
	}
	
	public static boolean validarComercioHistorial(String comercio) throws Exception{
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Historal", 0);
		List<WebElement> lstMovimientos = EFA.cv_driver.findElements(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell"));
		if(lstMovimientos.size() > 0){
			WebElement primero = lstMovimientos.get(0).findElement(By.xpath(".//XCUIElementTypeStaticText[1]"));
			String comercioPrimero = primero.getAttribute("label");
			if(comercioPrimero.equals(comercio))
			{
				primero.click();
				Reutilizable.tomarCapturaPantalla(500, "Detalle Pago");
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BACK_BLUE.getValue()));
				Reutilizable.tomarCapturaPantalla(500, "Movimientos");
				return true;
			}
		}		
		return false;
		
	}
	
	public static void validarListaComercios() throws Exception{
		if(GlobalData.getData("viValidarComercio").equals("SI")){
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			int nroComercios = EFA.cv_driver.findElements(By.xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell")).size();
			if(nroComercios <= 0){
				msjFinal = "No se ha mostrado ningún comercio";
			}
		}
	}
	
	private static Boolean validarTransaccionBD(String comercio, String monto, String email){
		try {
			String token = ValidacionBD.persona_ValidarTransaccion(comercio, monto, email);
			if(token.equals(Mensajes.DATANOTFOUND)) 
			{
				//msjFinal = "El resultado es nulo";
				return false;
			}
			GlobalData.setData("vOutEstadoTransaccion", token);
			System.out.println(token);
		} catch (Exception e) {
			//msjFinal = "Error en la conexi�n a la base de datos";
			GlobalData.setData("vOutEstadoTransaccion", e.getMessage());
			System.out.println(e.getMessage());
			return false;
		}
		return true;
	}
	
	public static Boolean escribirPropina() throws Exception {
		msjFinal = "";
		Element btnPorcentajePropina = null;
		String propina = GlobalData.getData("viPropina");
		Element  Xpropina = new Element("id","5%");
		boolean bTxtPropina = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Xpropina).toString());
		if(!propina.isEmpty()){
			if(!bTxtPropina){
				msjFinal = "La propina no esta habilitada para este comercio";
				return false;
			}
			if(propina.contains("%")){
				btnPorcentajePropina = new Element("xpath", "//XCUIElementTypeButton[@label='" + propina + "']");
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnPorcentajePropina).toString())){
					EFA.executeAction(Action.Click, btnPorcentajePropina);
				}
				else{
					msjFinal = "El porcentaje de propina indicado no es válido";
					return false;
				}
			}
			else{
				Element tip = new Element("xpath","//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeTextField");
				EFA.executeAction(Action.Click,tip);
				EFA.executeAction(Action.SendKeys,tip, propina);
				Reutilizable.hacerTap(327,131);
			}
		}
		
		return true;
	}

	public static String getMensaje() {
		return msjFinal;
	}
}
