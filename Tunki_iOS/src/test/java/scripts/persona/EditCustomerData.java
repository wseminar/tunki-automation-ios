package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.UtilitariosErrores;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;

public class EditCustomerData {
	private static String msjFinal = "";
	
	public static void customerData() throws Exception{
		Login_new.login();
		msjFinal = Login_new.getResult();
		if (!msjFinal.equals(Mensajes.SUCCESS))
			return;
		msjFinal = "";
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		EFA.cs_getTestEvidence("Menu mas", 1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TV_EDIT.getValue()));
		EFA.cs_getTestEvidence("Editar Persona", 1000);
		if(GlobalData.getData("viTipoFoto").equals("SELFIE")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TAKESELFIESPEC.getValue()));
			msjFinal = Reutilizable.selfiePhoto();
			if(!msjFinal.isEmpty()) return;
		}
		if(GlobalData.getData("viTipoFoto").equals("GALERIA")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.UPLOADPHOTOSPEC.getValue()));
			msjFinal = Reutilizable.galleryPhoto();
			if(!msjFinal.isEmpty()) return;
		}
		Thread.sleep(3000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return;
		EFA.cs_getTestEvidence("Menu Mas", 1000);
	}
	
	public static String getResult() throws Exception{
		if (!GlobalData.getData("viMensajeErrorEditar").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorEditar").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		return msjFinal.isEmpty() ? Mensajes.SUCCESS : msjFinal;
	}
	
}
