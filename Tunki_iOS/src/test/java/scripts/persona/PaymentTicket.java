package scripts.persona;

import com.everis.GlobalData;

import scripts.Util.UtilPaymentCommerce;
import scripts.helpers.Mensajes;
import scripts.helpers.ValidacionBD;

public class PaymentTicket {
	private static String msjFinal = "";
	private static String productId;
	private static String userId;
	private static String storeId;
	private static String commerceId;
	public static void payTicket() throws Exception{
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			String dato = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			System.out.println(dato);
			if(!dato.equals("DataNotFound")){
				ValidacionBD.eliminarTarjetaTblPMethod(dato);
				ValidacionBD.eliminarTarjetaTblPUser(dato);
				System.out.println("se borro tarjeta");
			}
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("SI")){
			String personId = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			System.out.println(personId);
			String paymentMethodId = ValidacionBD.obtenerPaymentMethodId(personId);
			System.out.println(paymentMethodId);
			String status = ValidacionBD.obtenerStatusTblPca(paymentMethodId);
			System.out.println(status);
			if(paymentMethodId.equals("DataNotFound") || !status.equals("0")){
				ValidacionBD.cambiarStatusTblPcaTD(paymentMethodId);
			}
		}
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		if(GlobalData.getData("viBdTicket").toUpperCase().equals("CREAR TICKET")){
			userId = ValidacionBD.obtenerUserIdComercio(GlobalData.getData("viUsuario"));
			if(userId.equals("DataNotFound")){
				msjFinal = "No hubo respuesta de BD";
			}
			storeId = ValidacionBD.obtenerStoreIdComercio(userId);
			if(storeId.equals("DataNotFound")){
				msjFinal = "No hubo respuesta de BD";
			}
			commerceId = ValidacionBD.obtenerCommerceIdComercio(storeId);
			if(commerceId.equals("DataNotFound")){
				msjFinal = "No hubo respuesta de BD";
			}
			productId = ValidacionBD.obtenerProductIdComercio(commerceId);
			System.out.println(productId);
			if(!productId.equals("DataNotFound")){
				ValidacionBD.cambiarStatusTblProduct(productId);
			}
			ValidacionBD.createTicketToday();
			String dato = ValidacionBD.verificacionCreacionTicket("5000001279", "0");
			if(dato.equals("DataNotFound")){
				msjFinal = "no se creo el ticket";
				return;
			}
		}
		msjFinal = "";
		
		if(!UtilPaymentCommerce.buscarComercio(GlobalData.getData("viComercio"))){
			msjFinal = "No se encontro el comercio.";
			return;
		}
		if(GlobalData.getData("viPagoSinTarjeta").toUpperCase().equals("SI")) {
			msjFinal = UtilPaymentCommerce.payNoCard();
			if(!msjFinal.isEmpty())return;	
		}
		msjFinal = UtilPaymentCommerce.paymentCommerceTicket();
		if(!msjFinal.isEmpty())return;	
		msjFinal = UtilPaymentCommerce.menuTicket();
		if(!msjFinal.isEmpty())return;	
		if(GlobalData.getData("viBdTicket").toUpperCase().equals("CREAR TICKET")){
			productId = ValidacionBD.obtenerProductIdComercio(commerceId);
			ValidacionBD.cambiarStatusTblProduct(productId);
		}
		msjFinal = UtilPaymentCommerce.menuMove();
		if(!msjFinal.isEmpty())return;	
		
	}
	
	public static String getResult(){
		if (!GlobalData.getData("viMensajeErrorPago").isEmpty()) {
			if (GlobalData.getData("viMensajeErrorPago").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
	
	}
	
}
