package scripts.persona;


import com.everis.GlobalData;
import scripts.Util.UtilLoginCustomer;
import scripts.helpers.Mensajes;


public class Login_new {
	private static String msjFinal = "";
	
	public static void login() throws Exception{
		msjFinal = "";
		msjFinal  = UtilLoginCustomer.loadingScreenLogin();
		if(!msjFinal.isEmpty())return;
		msjFinal = UtilLoginCustomer.enterYourUserName();
		if(!msjFinal.isEmpty())return;
		msjFinal = UtilLoginCustomer.loginPersona();
		if(!msjFinal.isEmpty())return;
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeError").isEmpty()){
			if(GlobalData.getData("viMensajeError").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;	
	}
}
