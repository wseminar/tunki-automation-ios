package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import scripts.Util.UtilPaymentCommerce;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.helpers.UtilitariosErrores;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class RecargaCelulares {
	private static String msjFinal = "";
	
	public static void recarga() throws Exception {
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		Thread.sleep(5000);
		EFA.cs_getTestEvidence("LISTA DE COMERCIOS", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MENU_LIST_SERVICE.getValue()));
		EFA.cs_getTestEvidence("SELECCIONA UN SERVICIO", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CELL_RECHARGE.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.hacerSwipe(245,610,76,-62);
		Thread.sleep(800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PHONE.getValue()),GlobalData.getData("viCelular"));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_AMOUNT.getValue()),GlobalData.getData("viMontoRecargar"));
		Reutilizable.hacerTap(336,77);
		Reutilizable.hacerTap(337, 364);
		Thread.sleep(3000);
		EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()) {
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				UtilPaymentCommerce.EleccionTarjetaCredito();
				break;
			case "DEBITO":
				UtilPaymentCommerce.EleccionTarjetaDebito();
			default:
				break;
			}
		}else {
			Reutilizable.hacerTap(261,159);
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.BIRD.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SHARE_INVOICE.getValue())).toString())) {
			 msjFinal = "No se realizo la recarga correctamente";
			 return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty())return;
		Reutilizable.hacerTap(235, 92);
		Thread.sleep(2000);
		EFA.cs_getTestEvidence("Constancia", 1000);
		
		
		
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorRecarga").equals("")) {
			if(GlobalData.getData("viMensajeErrorRecarga").trim().equals(msjFinal.trim()))
				return Mensajes.SUCCESS;
			else
				return "Los mensajes no son iguales: " + msjFinal;
		}			
		
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}
}
