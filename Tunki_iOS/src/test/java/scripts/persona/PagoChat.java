package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import script.textosTunki.Textos;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePxP;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;

public class PagoChat {
	public static String msjFinal = "";
	public static void pagoChat() throws Exception {
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";  
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		x.tap(112, 641).perform();
		EFA.cs_getTestEvidence("Lista de Contactos", 500);
		ReusablePxP.buscarPersona(GlobalData.getData("viPersonaCobrar"));
		EFA.cs_getTestEvidence("Chat", 500);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		int tries = 0;
		while(!ReusablePxP.capturarBtnPagar(Textos.RECIBISTE_UN_COBRO, "S/ "+GlobalData.getData("viMontoCobro"))){
			if(tries>6) {
				msjFinal = "No se obtuvo solicitud de pago";
			}
			Thread.sleep(15000);
			tries++;
		}
		msjFinal = ReusablePxP.irAPagar();
		if(!msjFinal.isEmpty()) return;
		System.out.println("tiempo");
		Thread.sleep(10000);
		System.out.println("paso tiempo");
		String monto = "- S/ "+GlobalData.getData("viMontoCobro");
		if(!GlobalData.getData("viModificarCobro").isEmpty()) {
			monto = "- S/ "+GlobalData.getData("viModificarCobro");
		}
		if(!ReusablePxP.capturarRespuestaChat(Textos.ENVIASTE_UN_PAGO,monto)) {
			msjFinal = "no muestra ultimo pago";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
		TouchAction z = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		z.press(91,375).moveTo(3,-136).release().perform();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.tomarCapturaPantalla(500, "Historial"); 
		if(GlobalData.getData("viValidarMovimiento").equals("SI")) {
			TouchAction s = new TouchAction((PerformsTouchActions)EFA.cv_driver);
			s.tap(155,91).perform();
			Reutilizable.tomarCapturaPantalla(300, "Constancia");	
		}
		
	
	
	
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorCobro").isEmpty()){
			if(GlobalData.getData("viMensajeError").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}		
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
	}
}
