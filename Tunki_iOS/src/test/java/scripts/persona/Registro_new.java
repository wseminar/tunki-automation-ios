package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import net.bytebuddy.agent.builder.AgentBuilder.CircularityLock.Global;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;
import javax.xml.bind.annotation.XmlElementDecl.GLOBAL;

import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePagoComercio;
import scripts.helpers.ReusablePxP;
import scripts.helpers.ReusableRegistroPersona;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class Registro_new {
	private static String msjFinal = "";

	private static Element optTipoDoc = new Element("xpath", "//XCUIElementTypeButton[@label='" + GlobalData.getData("viTipoDocumento") + "' and @visible='true']");
	
	public static void RegistroPersona() throws Exception{
		msjFinal = "";
		if(GlobalData.getData("viBorrarUsuarioBD").equals("SI")){
			String dato = ValidacionBD.obtenerPersonId(GlobalData.getData("viDocIdentidad"));
			System.out.println(dato);
			if(!dato.equals("DataNotFound")) {
				ValidacionBD.modificarDatosTblPerson(dato);
				ValidacionBD.modificarDatosTblUser(dato);
				System.out.println("se borro usuario");
			}
		}
		optTipoDoc = new Element("xpath", "//XCUIElementTypeButton[@label='" + GlobalData.getData("viTipoDocumento") + "' and @visible='true']");
		Thread.sleep(8000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_REGISTER.getValue()));
		msjFinal = ReusableRegistroPersona.ingresarDatosRegistrate();
		if(!msjFinal.isEmpty())return;
		if(GlobalData.getData("viNavegacion").equals("SI")) {
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),"D43CB9");
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ATRAS.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LOGIN.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Thread.sleep(2000);
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.VALIDA_TU_CORREO.getValue())).toString())) {
					msjFinal = "No muestra pop up valida tu correo";
				}
			String codigoVerificacion = ReusableRegistroPersona.ingresarCodigo();
			if(!codigoVerificacion.isEmpty()) return;
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue())).toString())) {
				msjFinal = "no muestra pantalla Completa tus datos";
				return;
			}
			msjFinal = ReusableRegistroPersona.ingresarDatos();
			if(!msjFinal.isEmpty()) return;
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ATRAS.getValue()));
			Thread.sleep(500);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LOGIN.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Thread.sleep(2000);
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.VALIDA_TU_CORREO.getValue())).toString())) {
					msjFinal = "No muestra pop up valida tu correo";
				}
				Thread.sleep(000);
				codigoVerificacion = ReusableRegistroPersona.ingresarCodigo();
				if(!codigoVerificacion.isEmpty()) return;
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue())).toString())) {
					msjFinal = "no muestra pantalla Completa tus datos";
					return;
				}
			msjFinal = ReusableRegistroPersona.ingresarDatos();
			if(!msjFinal.isEmpty()) return;
			String codigoSMS = ReusableRegistroPersona.ingresarCodigoSMS();
			if(!codigoSMS.isEmpty()) return;
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.ENTENDIDO.getValue())).toString())) {
				msjFinal = "no se registro correctamente";
				return;
			}
			return;
		}
		Thread.sleep(30000);
		msjFinal = ReusableRegistroPersona.ingresarCodigo();
		if(!msjFinal.isEmpty()) return;
		//Pantalla 2
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue())).toString())) {
			msjFinal = "no muestra pantalla Completa tus datos";
			return;
		}
		msjFinal = ReusableRegistroPersona.ingresarDatos();
		if(!msjFinal.isEmpty()) return;
		String codigoSMS = ReusableRegistroPersona.ingresarCodigoSMS();
		if(!codigoSMS.isEmpty()) return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.ENTENDIDO.getValue())).toString())) {
			msjFinal = "no se registro correctamente";
			return;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
		Thread.sleep(1000);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CONTINUAR_SIN_TARJETA.getValue())).toString())) {
			msjFinal = "no muestra pantalla agregar tarjeta";
			return;
		}
		if(GlobalData.getData("viAgregarTarjeta").equals("SI")){
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue())).toString())) {
					msjFinal = "No muestra pantalla Vincula tu Tarjeta";
					return;
				}
				msjFinal = ReusableRegistroPersona.AgregarTarjeta();
				if(!msjFinal.isEmpty())return;
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
				}
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_DESPUES.getValue())).toString())) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_DESPUES.getValue()));
				}
				Thread.sleep(10000);
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue())).toString())) {
					msjFinal = "No muestra listado de comercios";
					return;
				}
				if(GlobalData.getData("viP2C").equals("SI")) {
					if(!ReusablePagoComercio.buscarComercio(GlobalData.getData("viComercio"))){
						msjFinal = "no se encontro el comercio";
						return;
					}
					if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_STORE_NAME.getValue())).toString())){
						msjFinal = "no muestra pantalla de cobro";
						return;
					}
					msjFinal = ReusablePagoComercio.pagoComercio();
					if(!msjFinal.isEmpty()) return;
				}
				if(GlobalData.getData("viSesionActiva").equals("SI")){
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
					Reutilizable.tomarCapturaPantalla(500, "Menu Configuracion");
				}	
				if(GlobalData.getData("viPagoP2P").equals("SI")) {
					if(!ReusableRegistroPersona.pagoP2P())return;
				}	
				if(GlobalData.getData("viReIngreso").toUpperCase().equals("LOGIN")) {
					Element tab = new Element("id","group2905");
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SIGNOUT.getValue()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
					msjFinal = Reutilizable.validarErrores();
					if(!msjFinal.isEmpty()) return;
					EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_EMAIL.getValue()));
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viEmail2"));
					EFA.executeAction(Action.Click, tab);
					EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
					EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viPassword_2"));
					EFA.executeAction(Action.Click, tab);
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LOGIN.getValue()));
					EFA.cs_getTestEvidence("Pantalla Login", 0);
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
					msjFinal = Reutilizable.validarErrores();
					if(!msjFinal.isEmpty()) return;
				}
				return;
			}
		Reutilizable.hacerTap(319, 108);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CONTINUAR_SIN_TARJETA.getValue()));
		Thread.sleep(10000);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
			msjFinal = "No se realizo el registro";
			return;
		}
		Thread.sleep(1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		Thread.sleep(500);
		if(!msjFinal.isEmpty()) return;
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Thread.sleep(30000);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue())).toString())) {
			msjFinal = "No muestra listado de contactos";
			return;
		}
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return;
		if(GlobalData.getData("viPagoP2P").equals("SI")) {
			if(!ReusableRegistroPersona.pagoP2P())return;
		}	
		if(GlobalData.getData("viReIngreso").toUpperCase().equals("LOGIN")) {
			Element tab = new Element("id","group2905");
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SIGNOUT.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty()) return;
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_EMAIL.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viEmail2"));
			EFA.executeAction(Action.Click, tab);
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viPassword_2"));
			EFA.executeAction(Action.Click, tab);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LOGIN.getValue()));
			EFA.cs_getTestEvidence("Pantalla Login", 0);
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty()) return;
		}
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorRegistro").isEmpty()) {
			if(GlobalData.getData("viMensajeErrorRegistro").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "Mensaje no es igual: " + msjFinal;
		}
		
		return msjFinal.isEmpty() ? Mensajes.SUCCESS:msjFinal;
	}
	
	
	
	
}
