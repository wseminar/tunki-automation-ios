package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusablePxP;
import scripts.helpers.Reutilizable;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class PagoP2P {
	
	private static String msjFinal = "";
	
	public static void p2p() throws Exception{
		if(GlobalData.getData("viBorrarTarjeta").toUpperCase().equals("SI")){
			String dato = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			System.out.println(dato);
			if(!dato.equals("DataNotFound")){
				ValidacionBD.eliminarTarjetaTblPMethod(dato);
				ValidacionBD.eliminarTarjetaTblPUser(dato);
				System.out.println("se borro tarjeta");
			}
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("SI")){
			String personId = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
			System.out.println(personId);
			String paymentMethodId = ValidacionBD.obtenerPaymentMethodId(personId);
			System.out.println(paymentMethodId);
			String status = ValidacionBD.obtenerStatusTblPca(paymentMethodId);
			System.out.println(status);
			if(paymentMethodId.equals("DataNotFound") || !status.equals("0")){
				ValidacionBD.cambiarStatusTblPcaTD(paymentMethodId);
			}
		}
		if(GlobalData.getData("viQuitarValidacionTD").equals("AHORRO MILLONARIA")){
			String dni = ValidacionBD.obtenerDnixPersonId(GlobalData.getData("viEmail"));
			System.out.println(dni);
			ValidacionBD.cambiarStatusTblPcaMillonaria(dni);
		}
		Login_new.login();
		msjFinal = Login_new.getResult();
		if(!msjFinal.equals(Mensajes.SUCCESS)) return;
		msjFinal = "";
		Thread.sleep(5000);
		if(GlobalData.getData("viValidarMovimientoReceptor").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MENU_LIST_P2P_CONTACTS.getValue()));
			if(GlobalData.getData("viPantallaBusqueda").toUpperCase().equals("FAVORITOS")) {
				
			}
			if(GlobalData.getData("viPantallaBusqueda").toUpperCase().equals("CONTACTOS")) {
				ReusablePxP.buscarPersona(GlobalData.getData("viPersonaPagar"));
			}
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Reutilizable.tomarCapturaPantalla(500, "Historial"); 
			TouchAction a = new TouchAction((PerformsTouchActions)EFA.cv_driver);
			a.tap(155,91).perform();
			Reutilizable.tomarCapturaPantalla(300, "Constancia");	
		}
		EFA.cs_getTestEvidence("Lista de Comercio", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MENU_LIST_P2P_CONTACTS.getValue()));
		Thread.sleep(2000);
		EFA.cs_getTestEvidence("Lista Contactos", 500);
		ReusablePxP.buscarPersona(GlobalData.getData("viPersonaPagar"));
		msjFinal = Reutilizable.EnvioCobro();
		if(!msjFinal.isEmpty()) return;
		EFA.cs_getTestEvidence("Chat", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.tomarCapturaPantalla(500, "Historial"); 
		if(GlobalData.getData("viValidarMovimiento").equals("SI")) {
			TouchAction s = new TouchAction((PerformsTouchActions)EFA.cv_driver);
			s.tap(155,91).perform();
			Thread.sleep(5000);
			Reutilizable.tomarCapturaPantalla(800, "Constancia");	
		}
	}
	
	public static String getResult() throws Exception{
		if(!GlobalData.getData("viMensajeErrorCobro").equals("")) {
			if(GlobalData.getData("viMensajeErrorCobro").trim().equals(msjFinal.trim()))
				return Mensajes.SUCCESS;
			else
				return "Los mensajes no son iguales: " + msjFinal;
		}			
		
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}
}
