package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.Util.UtilLoginCustomer;
import scripts.helpers.Mensajes;
import scripts.helpers.ReusableOlvidoClavePersona;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class OlvidoPassword {

	private static String msjFinal = "";
	private static int n = 0 ;
	private static Element optTipoDoc = new Element("xpath", "//XCUIElementTypeButton[@label='" + GlobalData.getData("viTipoDocumento") + "' and @visible='true']");
	public static void PassPersona() throws Exception{
		msjFinal = "";
		optTipoDoc = new Element("xpath", "//XCUIElementTypeButton[@label='" + GlobalData.getData("viTipoDocumento") + "' and @visible='true']");
		n=0;
		msjFinal = UtilLoginCustomer.loadingScreenLogin();
		if(!msjFinal.isEmpty()) return;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_CAMBIAR_CUENTA.getValue()));
		Thread.sleep(1000);
		msjFinal = ReusableOlvidoClavePersona.validarPantallaIngresaTuCuenta();
		if(!msjFinal.isEmpty())return;
		if(!GlobalData.getData("viEmail").isEmpty()) {
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viEmail"));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.FORMATO_INCORRECTO.getValue())).toString())) {
			msjFinal = "Formato incorrecto";
				return;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.CAMPO_NECESARIO.getValue())).toString())) {
			msjFinal = "Campo necesario";
				return;
		}
		if(GlobalData.getData("viRealizarLogin").equals("SI")) {
			if(GlobalData.getData("viBloquearUsuario").equals("SI")) {
				for(int i=0;i<4;i++) {
					int n=0;
					for(int j=0;j<GlobalData.getData("viPassword_2").length();j++){
						EFA.executeAction(Action.Click,UtilLoginCustomer.idBoton(GlobalData.getData("viPassword_2").substring(n,n+1)));
						EFA.cs_getTestEvidence("Login",500);
						n++;
					}
					Thread.sleep(2000);
					if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RESTABLECER.getValue())).toString())) {
						msjFinal = ReusableOlvidoClavePersona.ValidarPopUpRestablecerPassword();
						if(!msjFinal.isEmpty()) return;
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RESTABLECER.getValue()));
						break;
					}
				}
			}
			if(GlobalData.getData("viPassword").length()<6) {
				int n = 0;
				for(int i=0;i<GlobalData.getData("viPassword").length();i++) {
					EFA.executeAction(Action.Click,UtilLoginCustomer.idBoton(GlobalData.getData("viPassword").substring(n,n+1)));
					EFA.cs_getTestEvidence("Login",500);
					n++;
				}
			}
			Thread.sleep(2000);
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RECOVER_PASSWORD.getValue())).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.RECOVER_PASSWORD.getValue()));
		}
		msjFinal = ReusableOlvidoClavePersona.validarPantallaIngresaDocumento();
		if(!msjFinal.isEmpty())return;
		if(!GlobalData.getData("viTipoDocumento").toUpperCase().equals("DNI")) {
		Reutilizable.hacerTap(75,354);
		Reutilizable.tomarCapturaPantalla(500, "TipoDocumento");
			switch (GlobalData.getData("viTipoDocumento").toUpperCase()) {
			case "CE":
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CE.getValue()));
				break;
			case "PASAPORTE":
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PASAPORTE.getValue()));
				break;	
			default:
				break;
			}
		}
		if(!GlobalData.getData("viDocIdentidad_2").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_DOCUMENT.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()),GlobalData.getData("viDocIdentidad_2"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENVIAR.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.validarErrores();
			if(msjFinal.equals(EMoreElements.OCURRIO_UN_ERROR_INESPERADO.toString())){
				return;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue())).toString())){
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
			}
		}
		if(!GlobalData.getData("viDocIdentidad").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()));
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()),GlobalData.getData("viDocIdentidad"));			
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENVIAR.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RESEND.getValue())).toString())){
			msjFinal = "No muestra pop up Valida tu Correo";
			return;
		}
		msjFinal = ReusableOlvidoClavePersona.validarPopUpValidaCorreo();
		if(!msjFinal.isEmpty())return;
		msjFinal = ReusableOlvidoClavePersona.ingresarCodigo();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD_2.getValue())).toString())){
			msjFinal = "No muestra pantalla Crea tu Conntraseña";
			return;
		}
		msjFinal = ReusableOlvidoClavePersona.validarPantallaCreaPassword();
		if(!msjFinal.isEmpty())return;
		if(!GlobalData.getData("viPass1").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viPass1"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue()));
		}
		if(!GlobalData.getData("viPass2").isEmpty()){
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD_2.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD_2.getValue()),GlobalData.getData("viPass2"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue()));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CONFIRMAR.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CONTINUAR.getValue())).toString())){
			msjFinal = "No muestra pantalla Contraseña Creada";
			return;
		}
		msjFinal = ReusableOlvidoClavePersona.validarPantallaPassCreada();
		if(!msjFinal.isEmpty())return;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CONTINUAR.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty())return;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.COMERCE.getValue())).toString())){
			msjFinal = "No muestra listado de Comercio";
			return;
		}
		EFA.cs_getTestEvidence("Listado de Comercio", 800);
		
		if(GlobalData.getData("viCerrarSesion").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SIGNOUT.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.validarErrores();
			if(!msjFinal.isEmpty())return;
			EFA.cs_getTestEvidence("Pantalla Login",800);
		}
		if(GlobalData.getData("viFlujosRecuperarPass").toUpperCase().equals("CREDENCIALES INVALIDAS")){
			EFA.cs_getTestEvidence("Pantalla Login", 800);
			int n = 0;
			for(int i = 0 ; i<GlobalData.getData("viPassword_2").length();i++){
				EFA.executeAction(Action.Click,UtilLoginCustomer.idBoton(GlobalData.getData("viPassword_2").substring(n,n+1)));
				EFA.cs_getTestEvidence("Login", 500);
				n++;
			}
			Thread.sleep(1000);
			EFA.cs_getTestEvidence("Error", 800);
			while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_0.getValue())).toString()));	
		}
		if(GlobalData.getData("viReLogin").equals("SI")){
			int n = 0;
			for(int i = 0 ; i<GlobalData.getData("viPassword").length();i++){
				EFA.executeAction(Action.Click,UtilLoginCustomer.idBoton(GlobalData.getData("viPassword").substring(n,n+1)));
				EFA.cs_getTestEvidence("Login", 500);
				n++;
			}
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.validarErrores();
			if(!msjFinal.isEmpty()) return;
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MENU_LIST_P2P_CONTACTS.getValue())).toString())){
				msjFinal = "No muestra listado de comercio";
				return;
			}
		}
		
	}
	
	public static String getResult(){
		if(!GlobalData.getData("viMensajeErrorPass").isEmpty()){
			if(GlobalData.getData("viMensajeErrorPass").equals(msjFinal))
				return Mensajes.SUCCESS;
			else
				return "El mensaje esperado no es igual: " + msjFinal;
		}
				
		return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
		
	}
}
