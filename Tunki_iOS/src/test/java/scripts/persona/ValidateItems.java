package scripts.persona;
import java.util.List;

import org.apache.tools.ant.taskdefs.optional.vss.MSVSSCHECKIN;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import net.bytebuddy.agent.builder.AgentBuilder.CircularityLock.Global;
import script.textosTunki.Textos;
import scripts.Util.UtilLoginCustomer;
import scripts.Util.UtilValitedateItemsCustomer;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;

public class ValidateItems {
		public static String msjFinal = "";

	public static void validarItems() throws Exception {
		Login_new.login();
		msjFinal = Login_new.getResult();
		if (!msjFinal.equals(Mensajes.SUCCESS))
			return;
		msjFinal = "";

		if(GlobalData.getData("viItemVincularTarjeta").toUpperCase().equals("SI")){
			msjFinal = UtilValitedateItemsCustomer.itemVincularTarjeta();
			if(!msjFinal.isEmpty())return;
			msjFinal = "";
		}
		if(GlobalData.getData("viItemMisTickets").toUpperCase().equals("SI")){
			msjFinal = UtilValitedateItemsCustomer.itemMisTickets();
			if(!msjFinal.isEmpty())return;
			msjFinal = "";
		}
	}
		public static String getResult() throws Exception{

			if (!GlobalData.getData("viMensajeError").isEmpty()) {
				if (GlobalData.getData("viMensajeError").equals(msjFinal))
					return Mensajes.SUCCESS;
				else
					return "El mensaje esperado no es igual: " + msjFinal;
			}
			return (msjFinal.isEmpty()) ? Mensajes.SUCCESS : msjFinal;
		
		}
}
