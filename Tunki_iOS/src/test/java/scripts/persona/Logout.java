package scripts.persona;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;

import scripts.helpers.Mensajes;

public class Logout {
	private static String msjFinal = "";
	
	private static Element progress = new Element("xpath", "//*[@type='XCUIElementTypeActivityIndicator']");	
	
	public static void logout() throws Exception{
		
		Element menuMas = new Element("xpath", "//XCUIElementTypeButton[@label='Más']");
		EFA.executeAction(Action.Click, menuMas);
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, progress).toString()));
		
		Element item_signout = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[7]");
		EFA.executeAction(Action.Click, item_signout);
		
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, progress).toString()));
		
	}
	
	public static String getResult() throws Exception{
		Element txtEmail = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[1]/XCUIElementTypeOther/XCUIElementTypeTextField");
		Element chkMantenerSesion = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeButton");
		Element txtPassword = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeSecureTextField");
		Element btnLogin = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton");
		if(!(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, txtEmail).toString())
				&& Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, chkMantenerSesion).toString())
				&& Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, txtPassword).toString())
				&& Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnLogin).toString()))){
				msjFinal = "Error: No se completó Logout correctamente";				
			}
		return (msjFinal.equals(""))? Mensajes.SUCCESS: msjFinal;
	}

}
