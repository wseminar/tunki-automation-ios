package scripts.helpers;

public class Mensajes {

	public static final String SUCCESS = "Ejecución Correcta";
	public static final String DATANOTFOUND = "DataNotFound";
	public static final String PAGOREALIZADO = "¡Pago Realizado!";
	public static final String COBROREALIZADO = "¡Cobro Realizado!";
	public static final String ENTENDIDO = "Entendido";
	public static final String DEVOLVER = "Devolver";
	public static final String CODIGOEXPIRADO = "El código de activación ha expirado.";
	public static final String CREDENCIALES_INVALIDA_TARJETA = "Tus credenciales no coinciden, por favor intenta nuevamente.";
	public static final String CREDENCIALESINVALIDAS_NEW = "Tus credenciales no coinciden.Si es tu primera vez en Tunki por favor regístrate.";
}
