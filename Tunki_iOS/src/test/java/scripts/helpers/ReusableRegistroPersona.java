package scripts.helpers;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.Util.UtilPaymentCommerce;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;
import scripts.persona.Firebase;
import scripts.persona.GestionCuenta;

public class ReusableRegistroPersona {
	 
	public static String ingresarDatosRegistrate() throws Exception{
		String msjFinal = "";
		if(!GlobalData.getData("viEmail").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viEmail"));
			Reutilizable.hacerTap(319, 108);
		}
		if(!GlobalData.getData("viPassword").isEmpty()) {
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viPassword"));
			Reutilizable.hacerTap(319, 108);
		}
		if(!GlobalData.getData("viConfirmPassword").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD.getValue()),GlobalData.getData("viConfirmPassword"));
			Reutilizable.hacerTap(319, 108);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LOGIN.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return msjFinal;
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue())).toString())) {
			return msjFinal = "No muestra pop up Valida tu correo";
			
		}
		return msjFinal;
		
	}
	
	public static String ingresarDatos() throws Exception {
		String msjFinal = "";
		Element optTipoDoc = new Element("xpath", "//XCUIElementTypeButton[@label='" + GlobalData.getData("viTipoDocumento") + "' and @visible='true']");
		Element tabphone = new Element ("id","img_Photo");
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue())).toString())) {
			msjFinal = "no muestra pantalla Completa tus datos";
			return msjFinal;
		}
		if(GlobalData.getData("viTipoFoto").equals("SELFIE")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue()));
			msjFinal = Reutilizable.selfiePhoto();
			if(!msjFinal.isEmpty()) return msjFinal;
		}
		if(GlobalData.getData("viTipoFoto").equals("GALERIA")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue()));
			msjFinal = Reutilizable.galleryPhoto();
			if(!msjFinal.isEmpty()) return msjFinal;
		}
		if(!GlobalData.getData("viNombre").isEmpty()) {
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME.getValue()),GlobalData.getData("viNombre"));
			Reutilizable.hacerTap(319, 108);
		}
		if(!GlobalData.getData("viApellido").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_LASTNAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_LASTNAME.getValue()),GlobalData.getData("viApellido"));
			Reutilizable.hacerTap(319, 108);
		}
		if(!GlobalData.getData("viTipoDocumento").equals("DNI")) {
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_DOCUMENT_TYPE.getValue()));
		Reutilizable.tomarCapturaPantalla(500, "TipoDocumento");
			EFA.executeAction(Action.Click, optTipoDoc);
		}
		if(!GlobalData.getData("viDocIdentidad").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()),GlobalData.getData("viDocIdentidad"));
			Reutilizable.hacerTap(319, 108);
		}
		if(!GlobalData.getData("viPhone").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PHONE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PHONE.getValue()),GlobalData.getData("viPhone"));
			EFA.executeAction(Action.Click, tabphone);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return msjFinal;
		return msjFinal;
	}
	
	public static String AgregarTarjeta() throws Exception {
		String msjFinal = "";
		if(GlobalData.getData("viTipoTarjeta").toUpperCase().equals("DEBITO")) {
			msjFinal = ingresarDatosTarjetaDebito();
			if(!msjFinal.isEmpty())return msjFinal;
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue())).toString())){
				if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("VARIAS CUENTAS".toUpperCase())) {
					return msjFinal = "";
				}
				EFA.cs_getTestEvidence("Listado de Cuentas",500);
				if(GlobalData.getData("viReIngresoGestionCuenta").toUpperCase().equals("DOS CUENTAS")) {
					EFA.cs_getTestEvidence("Seleccion Cuenta", 500);
					GestionCuenta.CapturarTextoPorId("Ahorro Millonaria Mn");
					EFA.cs_getTestEvidence("Listado de Cuentas",500);
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					msjFinal = Reutilizable.validarErrores();
					if(!msjFinal.isEmpty())return msjFinal;
				    if(GlobalData.getData("viValidarAhora").toUpperCase().equals("SI")) {
	    				msjFinal = UtilPaymentCommerce.validateTarjeta();
	    				if(!msjFinal.isEmpty())return msjFinal;
			    }
					return msjFinal;
				}
				TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
				x.press(234,469).moveTo(20,-308).release().perform();
				Thread.sleep(1000);
				Element item = new Element("xpath","(//XCUIElementTypeButton[@name='checkLoginUnSelected'])[6]");                          
				EFA.executeAction(Action.Click, item);
				EFA.cs_getTestEvidence("Seleccion Cuenta", 500);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
				Thread.sleep(25000);
				msjFinal = Reutilizable.validarErrores();
				if(!msjFinal.isEmpty())return msjFinal;
			}
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_AHORA.getValue())).toString())){
				return msjFinal = "No se realizo el registro de la tarjeta";
			}
		    if(GlobalData.getData("viValidarAhora").toUpperCase().equals("SI")) {
    				msjFinal = UtilPaymentCommerce.validateTarjeta();
    				if(!msjFinal.isEmpty())return msjFinal;
		    }
		}
		if(GlobalData.getData("viTipoTarjeta").equals("CREDITO")) {
			msjFinal = ingresarDatosTarjetaCredito();
			if(msjFinal.isEmpty())return msjFinal;
			return msjFinal;
		}
		
		
		return msjFinal;
		
	}
	
	public static String ingresarDatosTarjetaDebito() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
		if(!GlobalData.getData("viNroTarjeta").isEmpty()) {
			if(GlobalData.getData("viReIngreso").equals("SI GCP")) {
				if(GlobalData.getData("viBotonIntermitente").equals("SI")) {
					Reutilizable.hacerTap(319, 108);EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
				}
				if(!GlobalData.getData("viNroTarjeta_2").isEmpty()) {
					EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()),GlobalData.getData("viNroTarjeta_2"));
					Reutilizable.hacerTap(319, 108);
					}
			}
			EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()),GlobalData.getData("viNroTarjeta"));
			Reutilizable.hacerTap(319, 108);
		}
		if(!GlobalData.getData("viFechaVencimiento").isEmpty()) {
			if(GlobalData.getData("viReIngreso").equals("SI GCP")) {
				if(GlobalData.getData("viBotonIntermitente").equals("SI")) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
				}
				if(!GlobalData.getData("viFechaVencimiento_2").isEmpty()) {
					EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento_2"));
					Reutilizable.hacerTap(319, 108);
					}
			}
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento"));
			Reutilizable.hacerTap(319, 108);
			}
		if(!GlobalData.getData("viCVV").isEmpty()) {
			if(GlobalData.getData("viReIngreso").equals("SI GCP")) {
				if(GlobalData.getData("viBotonIntermitente").equals("SI")) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
				}
				if(!GlobalData.getData("viCVV_2").isEmpty()) {
					EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV_2"));
					Reutilizable.hacerTap(319, 108);
					}
			}
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV"));
			Reutilizable.hacerTap(319, 108);	
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue())).toString())) {
			if(!GlobalData.getData("viClaveWeb").isEmpty()) {
				if(GlobalData.getData("viReIngreso").equals("SI GCP")) {
					if(GlobalData.getData("viBotonIntermitente").equals("SI")) {
						EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
					}
					if(!GlobalData.getData("viClaveWeb_2").isEmpty()) {
						EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viClaveWeb_2"));
						Reutilizable.hacerTap(319, 108);
						}
				}
				EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viClaveWeb"));
				Reutilizable.hacerTap(319, 108);
				}
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
		if(GlobalData.getData("viValidarLabelsPantalla3").equals("SI")) {
			return msjFinal = "Validacion Textos";
		}
		Thread.sleep(30000);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		if(GlobalData.getData("viReIngresoGestionCuenta").equals("FORMA")) {
			msjFinal = reingresarDatosTarjeta();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.OCURRIO_UN_ERROR_INESPERADO.getValue())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
			Thread.sleep(1000);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		}
		msjFinal = Reutilizable.validarErrores();
		Thread.sleep(8000);
		if(msjFinal.trim().toUpperCase().equals(Mensajes.CREDENCIALES_INVALIDA_TARJETA.trim().toUpperCase())) {
			if(GlobalData.getData("viReIngresoGestionCuenta").equals("SI")) {
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
				msjFinal = reingresarDatosTarjeta();
				if(!msjFinal.isEmpty())return msjFinal;
			}
		}
		if(msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String ingresarDatosTarjetaCredito() throws Exception {
		String msjFinal = "";
		TouchAction z = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		z.press(296,589).moveTo(-2,-43).release().perform();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
		if(GlobalData.getData("viFlujoEspeciales").toUpperCase().equals("MANTENER DATOS")) {
			mantenerDatosIngresados();
			return msjFinal;
		}
		if(GlobalData.getData("viFlujoEspeciales").toUpperCase().equals("DATOS INCOMPLETOS")) {
			msjFinal = datosIncompletos();
			return msjFinal;
		}
	    if(!GlobalData.getData("viNroTarjeta").isEmpty()) {
	    		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()));
	    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()),GlobalData.getData("viNroTarjeta"));
	    		Reutilizable.hacerTap(319, 108);
	    		}
	    if(!GlobalData.getData("viFechaVencimiento").isEmpty()) {
	    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_DATE_EXPIRED.getValue()));
	    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento"));
	    		Reutilizable.hacerTap(319, 108);
	    		}
	    if(!GlobalData.getData("viCVV").isEmpty()) {
	    	EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_CVV.getValue()));
    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV"));
    		Reutilizable.hacerTap(319, 108);
    		}
	    EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
	    Thread.sleep(25000);
	    while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
	    msjFinal = Reutilizable.validarErrores();
	    if(!msjFinal.isEmpty())return msjFinal;
	    if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_AHORA.getValue())).toString())) {
	    		return msjFinal = "No se realizo el registro de la tarjeta correctamente";
	    }
	    if(GlobalData.getData("viValidarAhora").toUpperCase().equals("SI")) {
	    		msjFinal = UtilPaymentCommerce.validateTarjeta();
	    		if(!msjFinal.isEmpty())return msjFinal;
	    }
		return msjFinal;
	}
	
	public static String reingresarDatosTarjeta() throws Exception {
		String msjFinal = "";
		if(!GlobalData.getData("viNroTarjeta_2").isEmpty()) {
			EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()));
			EFA.executeAction(Action.SendKeys,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()),GlobalData.getData("viNroTarjeta_2"));
			Reutilizable.hacerTap(319, 108);
			}
		if(!GlobalData.getData("viFechaVencimiento_2").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento_2"));
			Reutilizable.hacerTap(319, 108);
			}
		if(!GlobalData.getData("viCVV_2").isEmpty()) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV_2"));
			Reutilizable.hacerTap(319, 108);
			}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue())).toString())) {	
		if(!GlobalData.getData("viClaveWeb_2").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viClaveWeb_2"));
			Reutilizable.hacerTap(319, 108);
			}
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
		Thread.sleep(25000);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.OCURRIO_UN_ERROR_INESPERADO.getValue())).toString())){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
			Thread.sleep(1000);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		}
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String ingresarCodigoSMS() throws Exception{
		String msjFinal = "";
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.VALIDA_TU_SMS.getValue())).toString())) {
			return msjFinal = "No muestra pop up valida telefono";
		}
		if(GlobalData.getData("viReenviarCodigoSMS").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ATRAS.getValue()));
			Thread.sleep(2000);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Thread.sleep(2000);
			if(!msjFinal.isEmpty()) return msjFinal;
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.VALIDA_TU_SMS.getValue())).toString())) {
				return msjFinal = "No muestra pop up valida telefono";
			}
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),GlobalData.getData("viCodigoSMS"));
			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
			Thread.sleep(4000);
			EFA.cs_getTestEvidence("Error Validar Telefono", 500);
			return msjFinal = "error campo codigo Telefono";
		}
		if(GlobalData.getData("viReenviarCodigoSMS").equals("NO")) {
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),GlobalData.getData("viCodigoSMS"));
			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
			Thread.sleep(4000);
			EFA.cs_getTestEvidence("Error Validar Telefono", 500);
			return msjFinal = "error campo codigo Telefono";
		}
		if(GlobalData.getData("viReenviarCodigoSMS").toUpperCase().equals("REENVIO FORMULARIO")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ATRAS.getValue()));
			msjFinal = reIngresarDatos();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		System.out.println("Ingresar a Firebase");
		if(!GlobalData.getData("viEmail2").isEmpty()) {
			String phone = GlobalData.getData("viPhone");
			String email = GlobalData.getData("viEmail2");
			Firebase.smsFirebase(phone,email);
		}
		if(!GlobalData.getData("viPhone2").isEmpty()) {
			String phone = GlobalData.getData("viPhone2");
			String email = GlobalData.getData("viEmail");
			Firebase.smsFirebase(phone,email);
		}
		if(GlobalData.getData("viEmail2").isEmpty() && GlobalData.getData("viPhone2").isEmpty()) {
			String phone = GlobalData.getData("viPhone");
			String email = GlobalData.getData("viEmail");
			Firebase.smsFirebase(phone,email);
		}
		Thread.sleep(20000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),"ABC123");
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
		EFA.cs_getTestEvidence("Validar Telefono", 500);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Thread.sleep(2000);
		if(!msjFinal.isEmpty()) return msjFinal;
		return msjFinal;
	}
	
	public static String ingresarCodigo() throws Exception{
		String msjFinal = "";
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.VALIDA_TU_CORREO.getValue())).toString())) {
			return msjFinal = "No muestra pop up valida tu correo";
		}
		if(GlobalData.getData("viReenviarCodigo").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ATRAS.getValue()));
			Thread.sleep(2000);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LOGIN.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Thread.sleep(2000);
			if(!msjFinal.isEmpty()) return msjFinal;
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.VALIDA_TU_CORREO.getValue())).toString())) {
					msjFinal = "No muestra pop up valida tu correo";
				}
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),GlobalData.getData("viCodigo"));
			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
			Thread.sleep(4000);
			EFA.cs_getTestEvidence("Error Validar Correo", 500);
			return msjFinal = "error campo codigo correo";
		}
		if(!GlobalData.getData("viCodigo").isEmpty()) {
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),GlobalData.getData("viCodigo"));
			EFA.cs_getTestEvidence("Ingresar Codigo", 500);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
			Thread.sleep(4000);
			EFA.cs_getTestEvidence("Error Validar Correo", 500);
			return msjFinal = "error campo codigo correo";
		}
		if(GlobalData.getData("viReenviarCodigo").toUpperCase().equals("REENVIO FORMULARIO")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ATRAS.getValue()));
			msjFinal=reIngresoDatosRegistrate();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		String codigo = obtenerCodigoVerificacionAlfa();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),codigo);
		EFA.cs_getTestEvidence("Ingresar Codigo", 500);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Thread.sleep(2000);
		if(!msjFinal.isEmpty()) return msjFinal;
		return msjFinal;
	}
	
	public static String obtenerCodigoVerificacionAlfa() throws Exception {
		String codigo = "";
		String primerDato = "";
		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i < 1; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "(?=.*[0-9])(?=.*[a-zA-Z]).{6,}";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}

	public static boolean pagoP2P() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MENU_LIST_P2P_CONTACTS.getValue()));
		Thread.sleep(5000);
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return false;
		ReusablePxP.buscarPersona(GlobalData.getData("viPersonaPagar"));
		msjFinal = Reutilizable.EnvioCobro();
		if(!msjFinal.isEmpty()) return false;
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		Reutilizable.tomarCapturaPantalla(500, "Historial"); 
		TouchAction s = new TouchAction((PerformsTouchActions)EFA.cv_driver);
		s.tap(155,91).perform();
		Reutilizable.tomarCapturaPantalla(300, "Constancia");	
		Reutilizable.tomarCapturaPantalla(300, "Constancia");
		return true;
	}
	
	public static String reIngresarDatos() throws Exception {
		String msjFinal = "";
		Element optTipoDoc = new Element("xpath", "//XCUIElementTypeButton[@label='" + GlobalData.getData("viTipoDocumento") + "' and @visible='true']");
		Element tab2 = new Element("id","group3");
		Element tabphone = new Element ("id","img_Photo");
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue())).toString())) {
			msjFinal = "no muestra pantalla Completa tus datos";
			return msjFinal;
		}
		if(GlobalData.getData("viTipoFoto2").equals("SELFIE")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue()));
			Reutilizable.selfiePhoto();
		}
		if(GlobalData.getData("viTipoFoto2").equals("GALERIA")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMERA.getValue()));
			Reutilizable.galleryPhoto();
			
		}
		if(!GlobalData.getData("viClienteNombre2").isEmpty()) {
			EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_NAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NAME.getValue()),GlobalData.getData("viClienteNombre2"));
			EFA.executeAction(Action.Click, tab2);
		}
		if(!GlobalData.getData("viClienteApellido2").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_LASTNAME.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_LASTNAME.getValue()),GlobalData.getData("viClienteApellido2"));
			EFA.executeAction(Action.Click, tab2);
		}
		if(!GlobalData.getData("viDocIdentidad_2").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DOCUMENT.getValue()),GlobalData.getData("viDocIdentidad_2"));
			EFA.executeAction(Action.Click, tab2);	
		}
		if(!GlobalData.getData("viPhone2").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PHONE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PHONE.getValue()),GlobalData.getData("viPhone2"));
			EFA.executeAction(Action.Click, tabphone);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.NEXT.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return msjFinal;
		return msjFinal;
	}
	
	public static String reIngresoDatosRegistrate() throws Exception {
		String msjFinal = "";
		Element tab = new Element ("id","group2");
		if(!GlobalData.getData("viEmail2").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viEmail2"));
			EFA.executeAction(Action.Click,tab);
		}
		if(!GlobalData.getData("viPassword_2").isEmpty()) {
			EFA.executeAction(Action.Clear,Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viPassword_2"));
			EFA.executeAction(Action.Click,tab);
		}
		if(!GlobalData.getData("viConfirmPassword_2").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD.getValue()),GlobalData.getData("viConfirmPassword_2"));
			EFA.executeAction(Action.Click, tab);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_LOGIN.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return msjFinal;
		return msjFinal;
	}
	
	public static void mantenerDatosIngresados() throws Exception {
		if(!GlobalData.getData("viNroTarjeta").isEmpty()) {
	    		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()));
	    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()),GlobalData.getData("viNroTarjeta"));
	    		Reutilizable.hacerTap(319, 108);
	    		}
	    if(!GlobalData.getData("viFechaVencimiento").isEmpty()) {
	    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_DATE_EXPIRED.getValue()));
	    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento"));
	    		Reutilizable.hacerTap(319, 108);
	    		}
	    if(!GlobalData.getData("viCVV").isEmpty()) {
	    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_CVV.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV"));
			Reutilizable.hacerTap(319, 108);
			}
	    TouchAction b = new TouchAction((PerformsTouchActions) EFA.cv_driver);
	    b.tap(339,178).perform();
	    b.press(309,550).moveTo(0,74).release().perform();
	    EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
	    if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue())).toString())) {
		if(!GlobalData.getData("viClaveWeb").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_PASSWORD.getValue()),GlobalData.getData("viClaveWeb"));
			Reutilizable.hacerTap(319, 108);
			}
	    }
		b.tap(339,178).perform();
		b.press(296,589).moveTo(-2,-43).release().perform();
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
	}
	
	public static String datosIncompletos() throws Exception {
		String msjFinal = "";
		if(!GlobalData.getData("viNroTarjeta_2").isEmpty()) {
    		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()));
    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()),GlobalData.getData("viNroTarjeta_2"));
    		Reutilizable.hacerTap(319, 108);
    		}
    if(!GlobalData.getData("viFechaVencimiento").isEmpty()) {
    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_DATE_EXPIRED.getValue()));
    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento"));
    		Reutilizable.hacerTap(319, 108);
    		}
    if(!GlobalData.getData("viCVV").isEmpty()) {
    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_CVV.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV"));
		Reutilizable.hacerTap(319, 108);
		}
    EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
	if(!GlobalData.getData("viNroTarjeta").isEmpty()) {
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue()),GlobalData.getData("viNroTarjeta"));
		Reutilizable.hacerTap(319, 108);
		}
	if(!GlobalData.getData("viFechaVencimiento_2").isEmpty()) {
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_DATE_EXPIRED.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento_2"));
		Reutilizable.hacerTap(319, 108);
		}
	EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
    if(!GlobalData.getData("viFechaVencimiento").isEmpty()) {
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_DATE_EXPIRED.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DATE_EXPIRED.getValue()),GlobalData.getData("viFechaVencimiento"));
		Reutilizable.hacerTap(319, 108);
		}
    if(!GlobalData.getData("viCVV_2").isEmpty()) {
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_CVV.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV_2"));
		Reutilizable.hacerTap(319, 108);
		}
    EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
    if(!GlobalData.getData("viCVV").isEmpty()) {
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_CVV.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CVV.getValue()),GlobalData.getData("viCVV"));
		Reutilizable.hacerTap(319, 108);
		}
    EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_NEXT.getValue()));
	while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
	msjFinal=Reutilizable.validarErrores();
	if(!msjFinal.isEmpty()) return msjFinal;
		return msjFinal;
	}
}
