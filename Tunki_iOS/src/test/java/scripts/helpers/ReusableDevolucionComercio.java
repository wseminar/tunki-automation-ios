package scripts.helpers;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;

public class ReusableDevolucionComercio {
	public static String Devolucion() throws Exception {
		String msjFinal = "";
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			Element pass = new Element("xpath","//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther");
			EFA.executeAction(Action.SendKeys,pass,GlobalData.getData("viPassword"));
			TouchAction m = new TouchAction((PerformsTouchActions) EFA.cv_driver);
			m.tap(206,371).perform();
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue())).toString())) {
				return msjFinal = "No se realizo la devolucion correctamente";
			}
			Reutilizable.tomarCapturaPantalla(500, "Historial"); 
			TouchAction s = new TouchAction((PerformsTouchActions)EFA.cv_driver);
			s.tap(169, 155).perform();
			Reutilizable.tomarCapturaPantalla(300, "Constancia");	
		return msjFinal; 
	}
}
