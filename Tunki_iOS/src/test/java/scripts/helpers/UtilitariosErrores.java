package scripts.helpers;

import com.everis.Action;
import com.everis.EFA;

import script.textosTunki.Textos;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class UtilitariosErrores {
	public static String validarErrorUps() throws Exception {
		String msjFinal = "";
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.OCURRIO_UN_ERROR_INESPERADO.getValue())).toString())){
			msjFinal = EMoreElements.OCURRIO_UN_ERROR_INESPERADO.getValue();
			EFA.cs_getTestEvidence("Error", 800);
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.UPS.getValue())).toString())){
			msjFinal = EMoreElements.UPS.getValue();
			EFA.cs_getTestEvidence("Error", 800);
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString())) {
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString();
			if(msjFinal.equals(Textos.PAGO_REALIZADO)) {
				return msjFinal = "";
			}else{
				return msjFinal;
			}
		}
		return msjFinal = "";
	}
	
	public static String validarErrorTickets() throws Exception{
		String msjFinal= "";
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EText.DESCRIPCION_ERROR_TICKETS.getValue())).toString())){
			msjFinal = EText.DESCRIPCION_ERROR_TICKETS.getValue();
			return msjFinal;
		}
		return msjFinal;
	}
	
	public static String validarErrorTextoComercio() throws InterruptedException, Exception{
		String msjFinal = "";
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.UPS.getValue())).toString())){
			msjFinal = EMoreElements.UPS.getValue();
			EFA.cs_getTestEvidence("Error", 800);
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EText.CREDENCIALES_INVALIDAS.getValue())).toString())){
			msjFinal = EText.CREDENCIALES_INVALIDAS.getValue();
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CAMPO_REQUERIDO.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CAMPO_REQUERIDO.getValue())).toString();
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INVALIDO.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INVALIDO.getValue())).toString();
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INVALIDO.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INVALIDO.getValue())).toString();
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CONTRASEÑA_NO_COINCIDE.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.CONTRASEÑA_NO_COINCIDE.getValue())).toString();
			return msjFinal;
		}
		return msjFinal = "";
	}
	
	
}
