package scripts.helpers;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.everis.PropertyReader;

public class Conector {

	public static Connection getOracleConnection() throws Exception{
		PropertyReader reader = new PropertyReader();
		reader.getProperties(new File("data").getAbsolutePath() + "/oracle.properties");
		Class.forName(reader.getProperty("dbClassName"));
		Properties props = new Properties();
		props.put("user",reader.getProperty("dbUser"));
		props.put("password",reader.getProperty("dbPassword"));
		Connection con = DriverManager.getConnection(reader.getProperty("dbStringConnection"), props);
		return con;		
	}
	
}
