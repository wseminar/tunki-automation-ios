package scripts.helpers;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;
import script.textosTunki.Textos;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class ReusableOlvidoClavePersona {
	
	public static String validarPantallaIngresaTuCuenta() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.LBL_TITLE.getValue())).toString().trim().toUpperCase().equals(Textos.INGRESA_TU_CUENTA.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.INGRESA_TU_CUENTA+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue())).toString().trim().toUpperCase().equals(Textos.ACEPTAR.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.ACEPTAR+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CANCEL.getValue())).toString().trim().toUpperCase().equals(Textos.CANCELAR.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.CANCELAR+"diferente";
		}
		
		return msjFinal;
	}
	
	public static String validarPantallaIngresaDocumento() throws Exception {
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().trim().toUpperCase().equals(Textos.DESCRIPCION_INGRESA_TU_DOCUMENTO.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.DESCRIPCION_INGRESA_TU_DOCUMENTO+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.BOTTOM_TEXT.getValue())).toString().trim().toUpperCase().equals(Textos.ADVERTENCIA_INGRESA_TU_DOCUMENTO.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.ADVERTENCIA_INGRESA_TU_DOCUMENTO+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENVIAR.getValue())).toString().trim().toUpperCase().equals(Textos.ENVIAR.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.ENVIAR+"diferente";
		}
		return msjFinal;
	}
	
	public static String validarPopUpValidaCorreo() throws Exception {
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.VALIDA_TU_CORREO.getValue())).toString().trim().toUpperCase().equals(Textos.VALIDA_TU_CORREO.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.VALIDA_TU_CORREO+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue())).toString().trim().toUpperCase().equals(Textos.ACEPTAR.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.ACEPTAR+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RESEND.getValue())).toString().trim().toUpperCase().equals(Textos.REENVIAR_CODIGO.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.REENVIAR_CODIGO+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CANCELAR.getValue())).toString().trim().toUpperCase().equals(Textos.CANCELAR.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.CANCELAR+"diferente";
		}
		return msjFinal;
	}
	
	public static String validarPantallaCreaPassword() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().trim().toUpperCase().equals(Textos.DESCRIPCION_CREA_TU_CONTRASEÑA.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.DESCRIPCION_CREA_TU_CONTRASEÑA+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.BOTTOM_TEXT.getValue())).toString().trim().toUpperCase().equals(Textos.ADVERTENCIA_CREA_TU_CONTRASEÑA.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.ADVERTENCIA_CREA_TU_CONTRASEÑA+"diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CONFIRMAR.getValue())).toString().trim().toUpperCase().equals(Textos.CONFIRMAR.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.CONFIRMAR+"diferente";
		}
		return msjFinal;
	}
	
	public static String ValidarPopUpRestablecerPassword() throws Exception{
		String msjFinal = "";
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.DETALLE_RESTABLECE_TU_CONTRASEÑA.getValue())).toString().trim().toUpperCase().equals(Textos.DETALLE_RESTABLECE_TU_CONTRASEÑA.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.DETALLE_RESTABLECE_TU_CONTRASEÑA+"diferente";
		}
		return msjFinal = "";
	}
	
	public static String validarPantallaPassCreada() throws Exception {
		String msjFinal = "";
		String x = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().trim().toUpperCase();
		System.out.println(x);
		System.out.println(Textos.DESCRIPCION_CONTRASEÑA_CREADA.toUpperCase().trim());
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_DETAIL.getValue())).toString().trim().toUpperCase().equals(Textos.DESCRIPCION_CONTRASEÑA_CREADA.toUpperCase().trim())) {
			return msjFinal = "Texto:"+Textos.DESCRIPCION_CONTRASEÑA_CREADA+" diferente";
		}
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CONTINUAR.getValue())).toString().trim().toUpperCase().equals(Textos.CONTINUAR.toUpperCase())) {
			return msjFinal = "Texto:"+Textos.CONTINUAR+" diferente";
		}
		return msjFinal;
	}
	
	public static String ingresarCodigo() throws Exception {
		String msjFinal = "";
		if(!GlobalData.getData("viCodigoVerificacion_2").isEmpty()){
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),GlobalData.getData("viCodigoVerificacion_2"));
			EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			msjFinal=Reutilizable.validarErrores();
			if(!msjFinal.isEmpty())return msjFinal;
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CONFIRM_PASSWORD_2.getValue())).toString())){
				return msjFinal = "No muestra pantalla Crea tu Conntraseña";
			}
		}
		if(GlobalData.getData("viFlujosRecuperarPass").toUpperCase().equals("BACK VALIDA CORREO")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CANCELAR.getValue()));
			Thread.sleep(800);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
			Thread.sleep(800);
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Element botonLogin = new Element ("id","btn_1");
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,botonLogin).toString())){
				return msjFinal = "No muestra pantalla de login";
			} 
			return msjFinal = "Flujo Correcto";
		}
		String codigoVerificacion = obtenerCodigoVerificacion();
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),codigoVerificacion);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal=Reutilizable.validarErrores();
		if(!msjFinal.isEmpty())return msjFinal;		
		return msjFinal;
	}
	
	public static String obtenerCodigoVerificacion() throws Exception {
		String codigo = "";
		String primerDato = "";

		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i <2; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "[0-9][0-9][0-9][0-9][0-9][0-9]";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
	
	
}
