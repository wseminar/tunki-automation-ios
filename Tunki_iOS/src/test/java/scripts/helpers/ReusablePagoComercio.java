package scripts.helpers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class ReusablePagoComercio {
	
	public static boolean buscarComercio(String comercio) throws Exception{
		EFA.cf_getTestEvidenceWithStep("LISTA DE COMERCIO", 500);
		EFA.cv_driver.findElementByXPath("(//XCUIElementTypeSearchField[@name='Buscar'])[1]").sendKeys(comercio.substring(0, comercio.length()-2));
		Thread.sleep(10000);
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.BUSCAR_COMERCIO.getValue()), comercio.substring(comercio.length()-2, comercio.length()));
		Thread.sleep(500);
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		List<WebElement> lista = EFA.cv_driver.findElements(By.id("txt_name"));
		for (WebElement item : lista) {
			if(item.getText().equals(comercio)){
				if(GlobalData.getData("viDetalleComercio").equals("SI")) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.IV_INFO.getValue()));
					Reutilizable.tomarCapturaPantalla(300, "Detalle Comercio");
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_PAY.getValue()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					Thread.sleep(500);
					EFA.cs_getTestEvidence("Espera Cobro", 0);
					return true;
				}
				item.click(); 
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Espera Cobro", 0);
				return true;
			}
		}
		return false;
	}
	
	public static String pagoComercio() throws Exception {
		String msjFinal = "";
		Element imgRocket = new Element("xpath", "//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther");
		int tries = 0;
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TV_STORE_NAME.getValue())).toString())) {
			if(tries==5)
			{
				return msjFinal = "No se recibió ninguna solicitud de cobro.";
			}
			Thread.sleep(15000);
			tries++;
		}
		msjFinal = escribirPropina();
		if(!msjFinal.isEmpty()) return msjFinal;	
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PAGAR.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, imgRocket).toString()));		
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.PAGO_REALIZADO.getValue())).toString())){
			if(Boolean.parseBoolean(GlobalData.getData(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString()))){
				return msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().trim();
			}
		}
		Reutilizable.tomarCapturaPantalla(300, "Pago Realizado");	
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ENTENDIDO.getValue()));
			Reutilizable.tomarCapturaPantalla(800, "Captura");
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
			while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
			Reutilizable.tomarCapturaPantalla(300, "Historial");
			if(GlobalData.getData("viValidarMovimiento").equals("SI")){
			TouchAction z = new TouchAction((PerformsTouchActions) EFA.cv_driver);
			z.tap(159,94).perform();
			Reutilizable.tomarCapturaPantalla(300, "Constancia");
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		}
		return msjFinal;
	}
	
	public static String escribirPropina() throws Exception {
		String msjFinal = "";
		Element btnPorcentajePropina = null;
		String propina = GlobalData.getData("viPropina");
		Element  Xpropina = new Element("id","5%");
		boolean bTxtPropina = Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Xpropina).toString());
		System.out.println(Xpropina);
		if(!propina.isEmpty()){
			if(!bTxtPropina){
				return msjFinal = "La propina no esta habilitada para este comercio";
			}
			if(propina.contains("%")){
				btnPorcentajePropina = new Element("xpath", "//XCUIElementTypeButton[@label='" + propina + "']");
				if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, btnPorcentajePropina).toString())){
					EFA.executeAction(Action.Click, btnPorcentajePropina);
				}
				else{
					return msjFinal = "El porcentaje de propina indicado no es válido";
				}
			}
			else{
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_TIP.getValue()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_TIP.getValue()), propina);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_OPERATION.getValue()));
			}
		}
		
		return msjFinal;
	}
}
