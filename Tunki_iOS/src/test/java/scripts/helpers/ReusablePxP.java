package scripts.helpers;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class ReusablePxP {
	public static String buscarPersona(String persona) throws Exception{
		String msjFinal = "";
		EFA.cs_getTestEvidence("Lista de Contactos", 500);
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.LOGO_CHAT_VACIO.getValue())).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTACTS.getValue()));
			Thread.sleep(10000);
			msjFinal = Reutilizable.validarErrores();
			if(!msjFinal.isEmpty()) return msjFinal;
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.BUSCAR_CONTACTO_FAVORITOS.getValue()),persona);
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_P2P_NAME.getValue()));
			return msjFinal;
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CONTACTS.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.BUSCAR_CONTACTO_FAVORITOS.getValue()),persona);
		//EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_P2P_NAME.getValue()));
		Thread.sleep(1000);
		Reutilizable.hacerTap(163,94);
		return msjFinal;
	}
	
	public static String envioCobroP2P(String monto) throws Exception {
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_COLLECT.getValue()));
		EFA.cs_getTestEvidence("Pantalla Cobrar", 500);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_AMOUNT.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_AMOUNT.getValue()),monto);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IMG_PHOTO.getValue()));
		if(GlobalData.getData("viAgregarDescripcion").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_DETAIL.getValue()));
			EFA.executeAction(Action.SendTEKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DETAIL.getValue()),GlobalData.getData("viDescripcionPago"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IMG_PHOTO.getValue()));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return msjFinal;
		return msjFinal;
	}
	
	public static boolean capturarRespuestaChat(String textoChat,String montoCobro) throws Exception {
		for(int i=10;i>7;i--){
			List<WebElement> lstMovimientosChat = EFA.cv_driver.findElementsByXPath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["+i+"]");
			boolean elemento = Boolean.parseBoolean(EFA.executeAction(Action.GetAttribute, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.POSICION_CHAT.getValue()),"visible").toString());
			if(!elemento) {
				TouchAction s = new TouchAction((PerformsTouchActions) EFA.cv_driver);
				s.press(324,578).moveTo(-1,-399).release().perform();
				s.press(324,578).moveTo(-1,-399).release().perform();
			}
			for(WebElement webElement : lstMovimientosChat) {
				WebElement title = webElement.findElement(By.name("lbl_Message"));
				String xtitle = title.getText().trim();
				System.out.println(xtitle);
				WebElement monto = webElement.findElement(By.name("lbl_amount"));
				String xmonto = monto.getText().trim();
				System.out.println(xmonto);
				if(xtitle.equals(textoChat) && xmonto.equals(montoCobro)){
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean capturarBtnPagar(String textoChat,String montoCobro) throws Exception {
		for(int i=10;i>8;i--){
			boolean elemento = Boolean.parseBoolean(EFA.executeAction(Action.GetAttribute, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.POSICION_CHAT.getValue()),"visible").toString());
			if(!elemento) {
				TouchAction s = new TouchAction((PerformsTouchActions) EFA.cv_driver);
				s.press(324,578).moveTo(-1,-399).release().perform();
				s.press(324,578).moveTo(-1,-399).release().perform();
				Thread.sleep(3000);
			}
			Thread.sleep(3000);
			EFA.cs_getTestEvidence("Chat", 500);
			List<WebElement> lstMovimientosChat = EFA.cv_driver.findElementsByXPath("//XCUIElementTypeApplication/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell["+i+"]");
			for(WebElement webElement : lstMovimientosChat) {
				WebElement btnPagar = webElement.findElement(By.name("btn_gotoPay"));
				WebElement title = webElement.findElement(By.name("lbl_Message"));
				String xtitle = title.getText().trim();
				System.out.println(xtitle);
				WebElement monto = webElement.findElement(By.name("lbl_amount"));
				String xmonto = monto.getText().trim();
				System.out.println(xmonto);
				if(xtitle.equals(textoChat) && xmonto.equals(montoCobro)){
					btnPagar.click();
					return true;
				}
			}
		}
		return false;
	}
	
	public static String irAPagar() throws Exception {
		String msjFinal = "";
		EFA.cs_getTestEvidence("Cobro", 500);
		if(!GlobalData.getData("viModificarCobro").isEmpty()) {
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_AMOUNT.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_AMOUNT.getValue()),GlobalData.getData("viModificarCobro"));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IMG_PHOTO.getValue()));
		EFA.cs_getTestEvidence("Cobro", 500);
		if(GlobalData.getData("viAgregarDescripcion").equals("SI")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DETAIL.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DETAIL.getValue()),GlobalData.getData("viDescripcionPago"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IMG_PHOTO.getValue()));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.IMGROCKET.getValue())).toString()));
		EFA.cs_getTestEvidence("Constancia", 500);
		msjFinal = Reutilizable.validarErrores();
		if(!msjFinal.isEmpty()) return msjFinal;
		EFA.cs_getTestEvidence("Constancia", 500);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		return msjFinal;
		
	}
	
}
