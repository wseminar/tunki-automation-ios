package scripts.helpers;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FromTerm;
import javax.mail.search.SearchTerm;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.everis.*;

import script.textosTunki.Textos;
import scripts.Util.UtilPaymentCommerce;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;

public class Reutilizable {
	
	public static String validarErrores() throws Exception{
		String msjFinal = ""; 
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(),EMoreElements.OCURRIO_UN_ERROR_INESPERADO.getValue())).toString())){
			msjFinal = EMoreElements.OCURRIO_UN_ERROR_INESPERADO.toString();
			tomarCapturaPantalla(300, "Cuenta ya existe");
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.LBL_DETAIL.getValue())).toString())) {
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.LBL_DETAIL.getValue())).toString();
			System.out.println(msjFinal);
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.TARJETA_NO_OPERATIVA.getValue())).toString())) {
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.TARJETA_NO_OPERATIVA.getValue())).toString();
			System.out.println(msjFinal);
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.DATOS_EXISTENTES_COMERCIO.getValue())).toString())) {
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.DATOS_EXISTENTES_COMERCIO.getValue())).toString();
			return msjFinal ;
		}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.CREDENCIALES_INCORRECTAS.getValue())).toString())) {
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.CREDENCIALES_INCORRECTAS.getValue())).toString();
				return msjFinal ;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.CAMPO_REQUERIDO.getValue())).toString())){
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.CAMPO_REQUERIDO.getValue())).toString();
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INVALIDO.getValue())).toString())){
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INVALIDO.getValue())).toString();
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EText.PASS_DIFERENTE.getValue())).toString())){
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EText.PASS_DIFERENTE.getValue())).toString();
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.CONTRASEÑA_NO_COINCIDE.getValue())).toString())){
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.CONTRASEÑA_NO_COINCIDE.getValue())).toString();
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), ETextArea.TXT_MESSAGE.getValue())).toString())){
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(),ETextArea.TXT_MESSAGE.getValue())).toString();
				tomarCapturaPantalla(300, "PoP UP Error");
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.UPS.getValue())).toString())){
				msjFinal = EMoreElements.UPS.getValue();
				tomarCapturaPantalla(300, "PoP UP Error");
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(),EMoreElements.CUENTA_EXISTE.getValue())).toString())){
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.CUENTA_EXISTE.getValue())).toString();
				tomarCapturaPantalla(300, "Cuenta ya existe");
				return msjFinal;
			}
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.ERROR_ENVIO_CODIGO.getValue())).toString())) {
				msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.ERROR_ENVIO_CODIGO.getValue())).toString();
				return msjFinal;
			}
		
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.CUENTA_VINCULADA_IBK.getValue())).toString())) {
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.CUENTA_VINCULADA_IBK.getValue())).toString();
			tomarCapturaPantalla(300, "Cuenta ya existe");
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.YA_HAS_AGREGADO_ESTA_TARJETA.getValue())).toString())) {
			msjFinal = "Ya has agregado esta tarjeta.";
			tomarCapturaPantalla(300, "Cuenta ya existe");
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(),EMoreElements.USUARIO_REGISTRADO.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.USUARIO_REGISTRADO.getValue())).toString();
			tomarCapturaPantalla(300, "Cuenta ya existe");
			return msjFinal;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.CAMPO_NECESARIO.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.CAMPO_NECESARIO.getValue())).toString();
			return msjFinal ;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INCORRECTO.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.FORMATO_INCORRECTO.getValue())).toString();
			return msjFinal ;
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, newElement(EComponents.ID.getValue(),EMoreElements.ITENTA_NUEVAMENTE.getValue())).toString())){
			msjFinal = EFA.executeAction(Action.GetText, newElement(EComponents.ID.getValue(), EMoreElements.ITENTA_NUEVAMENTE.getValue())).toString();
			tomarCapturaPantalla(300, "Cuenta ya existe");
			return msjFinal;
		}

		return msjFinal;
	}
	
	public static DesiredCapabilities getCapabilities() {
		PropertyReader reader = new PropertyReader();
		reader.getProperties(new File("data").getAbsolutePath() + "/capabilities.properties");
		
		DesiredCapabilities capabilities = DesiredCapabilities.iphone();	
        capabilities.setCapability("platformName", GlobalData.getData("PlatformName"));
        capabilities.setCapability("automationName", "XCUITest");
        capabilities.setCapability("deviceName", GlobalData.getData("DeviceName"));
        capabilities.setCapability("udid", GlobalData.getData("DeviceID"));
        capabilities.setCapability("platformVersion", GlobalData.getData("PlatformVersion"));
        capabilities.setCapability("bundleId", GlobalData.getData("AppPackage"));
        capabilities.setCapability("xcodeSigningId", reader.getProperty("xcodeSigningId"));
        capabilities.setCapability("xcodeOrgId", reader.getProperty("xcodeOrgId"));
        capabilities.setCapability("autoGrantPermissions", true);
        capabilities.setCapability("--session-override", true);
        capabilities.setCapability("noReset", true);
        if(reader.getProperty("useNewWDA").equals("TRUE"))
        		capabilities.setCapability("useNewWDA", true);
        else
        		capabilities.setCapability("useNewWDA", false);
        if(!reader.getProperty("newCommandTimeout").equals("0"))
        		capabilities.setCapability("newCommandTimeout", Integer.parseInt(reader.getProperty("newCommandTimeout")));
        
        return capabilities;
	}
	
	public static Element newElement(String identifier,String element) {
		return new Element(identifier,element); 
	}
	
	public static void tomarCapturaPantalla(long milisegundos, String nombreCaptura) throws InterruptedException {
		Thread.sleep(milisegundos);
		EFA.cs_getTestEvidence(nombreCaptura, 0);
	}
	
	public static String obtenerTipoTarjetaFinal(){
		String tipoTarjeta = GlobalData.getData("viTipoTarjeta");
		if(GlobalData.getData("viReIngresoGestionCuenta").equals("SI") && !GlobalData.getData("viTipoTarjeta_2").isEmpty())
			tipoTarjeta = GlobalData.getData("viTipoTarjeta_2");
		return tipoTarjeta;
	}
	
	public static String seleccionarTipoTarjeta(String tipoTarjeta) throws Exception {
		String msjFinal = "";
		switch (tipoTarjeta.toUpperCase()) {
		case "DEBITO":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TARJETA_DEBITO.getValue()));
			break;
		case "CREDITO":
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.TARJETA_CREDITO.getValue()));
			break;
		default:
			msjFinal = "Ingresar un tipo de tarjeta correcto (DEBITO O CREDITO)";
			return msjFinal;
		}
		return msjFinal;
	}
	
	public static void ingresarDatosCredito() throws Exception{
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CVV.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CVV.getValue()), GlobalData.getData("viCVV"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.IMAGE_VIEW.getValue()));
		
		if(GlobalData.getData("viBotonIntermitente").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			if(!GlobalData.getData("viCVV_2").isEmpty())
			{
				EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CVV.getValue()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_CVV.getValue()), GlobalData.getData("viCVV_2"));
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.IMAGE_VIEW.getValue()));
			}
		}
		EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EXPIRATION_DATE.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EXPIRATION_DATE.getValue()), GlobalData.getData("viFechaVencimiento"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.IMAGE_VIEW.getValue()));
		
		if(GlobalData.getData("viBotonIntermitente").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			if(!GlobalData.getData("viFechaVencimiento_2").isEmpty())
			{
				EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EXPIRATION_DATE.getValue()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_EXPIRATION_DATE.getValue()), GlobalData.getData("viFechaVencimiento_2"));
				EFA.executeAction(Action.Click,Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.IMAGE_VIEW.getValue()));
			}
		}
	}
	
	public static void ingresarDatosDebito() throws Exception{
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_BPI.getValue()));
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_BPI.getValue()), GlobalData.getData("viClaveWeb"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.IMAGE_VIEW.getValue()));
		if(GlobalData.getData("viBotonIntermitente").equals("SI")){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			if(!GlobalData.getData("viClaveWeb_2").isEmpty())
			{
				EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_BPI.getValue()));
				EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_PASSWORD_BPI.getValue()), GlobalData.getData("viClaveWeb_2"));
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.IMAGE_VIEW.getValue()));
			}
		}
	}
	
	public static String navegarMenuGestionCuentas() throws Exception{
		String msjFinal="";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		Reutilizable.tomarCapturaPantalla(500, "Menú Más");
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_ACCOUNT_MANAGMENT.getValue()));
		Reutilizable.tomarCapturaPantalla(500, "Gestión de Cuentas");
		return msjFinal;
	}
	
	public static void BorrarCampo() throws Exception {
		for(int i = 0;i<1;i++) {
			EFA.executeAction(Action.Click,newElement(EComponents.ID.getValue(), EMoreElements.DELETE.getValue()));
		}
	}
	
	public static String EnvioCobro() throws Exception{
		String msjFinal= "";
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PAY.getValue())).toString())) {
			return msjFinal = "No muestra chat";
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PAY.getValue()));
		if(GlobalData.getData("viPagoSinTarjeta").toUpperCase().equals("SI")) {
			msjFinal = UtilPaymentCommerce.payNoCard();
			if(!msjFinal.isEmpty())return msjFinal;	
		}
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_AMOUNT.getValue())).toString())) {
			return msjFinal = "No muestra pantalla pago";
		}
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.ET_AMOUNT.getValue()), GlobalData.getData("viMontoCobro"));
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.IMG_PHOTO.getValue()));
		Reutilizable.hacerTap(336,335);
		Thread.sleep(3000);
		EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()) {
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				UtilPaymentCommerce.EleccionTarjetaCredito();
				break;
			case "DEBITO":
				UtilPaymentCommerce.EleccionTarjetaDebito();
			default:
				break;
			}
		}else {
			Reutilizable.hacerTap(261,159);
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		}
		if(GlobalData.getData("viAgregarDescripcion").equals("SI")){
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_DETAIL.getValue()),GlobalData.getData("viDescripcionPago"));
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.IMG_PHOTO.getValue()));
		}
		Reutilizable.hacerTap(261,159);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		if(GlobalData.getData("viValidarAhora").toUpperCase().equals("PAGO")) {
			EFA.cs_getTestEvidence("Pantalla Validacion", 1000);
			msjFinal = UtilPaymentCommerce.validateTarjeta();
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
				msjFinal = "";
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
				EFA.cs_getTestEvidence("pago p2p", 1500);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.BIRD.getValue())).toString()));
				if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().equals(Textos.PAGO_REALIZADO)){
					return msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().trim();
				}
				Thread.sleep(10000);
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SHARE_INVOICE.getValue())).toString())) {
					return msjFinal = "No se realizo la transferencia";
				}
				EFA.cs_getTestEvidence("Pago Realizado5", 1000);
				EFA.cs_getTestEvidence("Pago Realizado6", 1000);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
				return msjFinal;
			}else {
				return msjFinal;
			}
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.BIRD.getValue())).toString()));
		if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().equals(Textos.PAGO_REALIZADO)){
			return msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().trim();
		}
		Thread.sleep(10000);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_SHARE_INVOICE.getValue())).toString())) {
			return msjFinal = "No se realizo la transferencia";
		}
		EFA.cs_getTestEvidence("Pago Realizado5", 1000);
		EFA.cs_getTestEvidence("Pago Realizado6", 1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		return msjFinal;
	}
	
	public static String selfiePhoto() throws Exception {
		String msjFinal = "";
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_TAKE_PICTURE.getValue())).toString())) {
			EFA.executeAction(Action.Click,newElement(EComponents.ID.getValue(), EButtons.BTN_TAKE_PICTURE.getValue()));
			Thread.sleep(2000);
		}
		EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.PHOTO_CAPTURE.getValue()));
		if(GlobalData.getData("viFlujoEditar").equals("CANCELAR FOTO")) {
			EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.RETAKE.getValue()));
			Thread.sleep(2000);
			hacerTap(35,617);
			return msjFinal;
		}
		EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.USE_PHOTO.getValue()));
		EFA.cs_getTestEvidence("Foto Selfie", 500);
		EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.DONE.getValue()));
		return msjFinal;
	}
	
	public static String galleryPhoto() throws Exception {
		String msjFinal = "";
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_GALLERY.getValue())).toString())) {
			EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.BTN_OPEN_GALLERY.getValue()));
		}
		TouchAction x = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		EFA.cs_getTestEvidence("Foto Galeria", 500);
		Thread.sleep(3000);
		x.tap(178,107).perform();
		Thread.sleep(3000);
		x.tap(46,155).perform();
		EFA.cs_getTestEvidence("Foto Galeria", 500);
		Thread.sleep(3000);
		if(GlobalData.getData("viFlujoEditar").equals("CANCELAR FOTO")) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.CANCEL.getValue()));
			return msjFinal;
		}
		EFA.executeAction(Action.Click, newElement(EComponents.ID.getValue(), EButtons.DONE.getValue()));
		return msjFinal;
	}
	
	public static void hacerSwipe (int x , int y , int a , int b) {
		TouchAction swipe = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		swipe.press(x, y).moveTo(a, b).release().perform();
	}
	
	public static void hacerTap (int x,int y) {
		TouchAction xTap = new TouchAction((PerformsTouchActions) EFA.cv_driver);
		xTap.tap(x, y).perform();
	}
	
	public static String obtenerCodigoVerificacionAlfa() throws Exception {
		String codigo = "";
		String primerDato = "";
		boolean primerIntento = false;

		Properties props = new Properties();
		String host = "smtp.gmail.com";
		String username = GlobalData.getData("viEmail");
		String password = "auto2017.";
		String provider = "imaps";

		Session session = Session.getDefaultInstance(props, null);
		Store store = session.getStore(provider);
		store.connect(host, username, password);

		for (int i = 0; i < 1; i++) {
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			SearchTerm sender = new FromTerm(new InternetAddress("tunki"));
			Message[] messages = inbox.search(sender);
			for (Message message : messages) {
				codigo = getTextFromMessage(message);
			}
			String regex = "(?=.*[0-9])(?=.*[a-zA-Z]).{6,}";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(codigo);

			if (!primerIntento) {
				if (m.find()) {
					primerDato = m.group(0);
					codigo = m.group(0);
				}
				primerIntento = true;
			} else {
				if (m.find()) {
					codigo = m.group(0);
				}
				if (!codigo.equals(primerDato)) {
					inbox.close(false);
					store.close();
					break;
				}
			}
			System.out.println(primerDato + " " + codigo);

			Thread.sleep(10000);
		}

		return codigo;
	}

	private static String getTextFromMessage(Message message) throws MessagingException, IOException {
		String result = "";
		if (message.isMimeType("text/plain")) {
			result = message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			result = getTextFromMimeMultipart(mimeMultipart);
		}
		return result;
	}

	private static String getTextFromMimeMultipart(MimeMultipart mimeMultipart) throws MessagingException, IOException {
		String result = "";
		int count = mimeMultipart.getCount();
		for (int i = 0; i < count; i++) {
			BodyPart bodyPart = mimeMultipart.getBodyPart(i);
			if (bodyPart.isMimeType("text/plain")) {
				result = result + "\n" + bodyPart.getContent();
				break; // without break same text appears twice in my tests
			} else if (bodyPart.isMimeType("text/html")) {
				String html = (String) bodyPart.getContent();
				result = result + "\n" + html;
			} else if (bodyPart.getContent() instanceof MimeMultipart) {
				result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
			}
		}
		return result;
	}
	

}
