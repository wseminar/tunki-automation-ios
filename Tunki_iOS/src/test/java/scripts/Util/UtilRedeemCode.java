package scripts.Util;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import scripts.helpers.Reutilizable;
import scripts.helpers.UtilitariosErrores;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class UtilRedeemCode {
	
	public static String menuTicket() throws Exception {
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EBarButtons.MORE.getValue()));
		EFA.cs_getTestEvidence("Menu mas", 1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_TICKETS.getValue()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if(!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String popUpRedeemCode() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.IMAGE_CODE.getValue()));
//		msjFinal = textPopUpRedeemCode();
//		if(!msjFinal.isEmpty()) return msjFinal;
		String codigoTicket = codeTicket();
		if(!GlobalData.getData("viCodigoTicket").isEmpty()){
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),GlobalData.getData("viCodigoTicket"));
		}else {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_CODE.getValue()),codigoTicket);
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VERIFICAR.getValue()));
		while (Boolean.parseBoolean(EFA
				.executeAction(Action.IsElementPresent,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
				.toString()))
			;
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return msjFinal;
		return msjFinal;
	}
	
	public static String screenRedeemTicket() throws Exception{
		String msjFinal= "";
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue())).toString())) {
			return msjFinal = "No muestra pantalla canje ticket";
		}
		EFA.cs_getTestEvidence("Canje ticket", 1000);
		return msjFinal;
	}
	
	public static String codeTicket() throws Exception{
		String msjFinal = "";		
		String userId = ValidacionBD.obtenerUserIdComercio(GlobalData.getData("viUsuario"));
		System.out.println(userId);
		if(userId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String storeId = ValidacionBD.obtenerStoreIdComercio(userId);
		System.out.println(storeId);
		if(storeId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String commerceId = ValidacionBD.obtenerCommerceIdComercio(storeId);
		System.out.println(commerceId);
		if(commerceId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String productId = ValidacionBD.obtenerProductIdComercio(commerceId);
		System.out.println(productId);
		if(productId.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		String codigoTicket = ValidacionBD.obtenerCodigoTicket(productId,GlobalData.getData("viEmail"));
		System.out.println(codigoTicket);
		if(codigoTicket.equals("DataNotFound")){
			return msjFinal = "No hubo respuesta de BD";
		}
		msjFinal = codigoTicket;
		return msjFinal;
	}
}
