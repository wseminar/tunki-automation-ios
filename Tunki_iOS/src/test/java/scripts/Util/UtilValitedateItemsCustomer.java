package scripts.Util;

import java.util.List;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import org.openqa.selenium.WebElement;
import script.textosTunki.Textos;
import scripts.helpers.Mensajes;
import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class UtilValitedateItemsCustomer {
	// metodos que se encargan de validar los textos y funcionalidad de los bot�nes

	public static String itemVincularTarjeta() throws Exception {
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		String textoItemVincular = EFA.executeAction(Action.GetText,Reutilizable.newElement(EComponents.XPATH.getValue(),EText.ITEM_VINCULAR_TARJETA.getValue())).toString();
		if (textoItemVincular.equals(Textos.VINCULA_TARJETA)) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_ACCOUNT_MANAGMENT.getValue()));
			System.out.println("Click en " + textoItemVincular);
			if (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_ACEPTAR_TARJETA.getValue()))
					.toString())) {
				
				EFA.executeAction(Action.Click,
						Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACEPTAR_TARJETA.getValue()));
				
				if (GlobalData.getData("viAccionBackItem").toUpperCase().equals("SI")) {
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
					
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.COMERCE.getValue()));
					msjFinal = Mensajes.SUCCESS;
				}
			} else {
				msjFinal = "No se encontro scroll seleccion de tarjeta";
			}
		} else {
			msjFinal = "El texto del item no coincide";
		}

		return msjFinal;
	}
	public static String itemMisTickets() throws Exception {
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		String textoItemMisTicket = EFA
				.executeAction(Action.GetText,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EText.ITEM_MIS_TICKETS.getValue()))
				.toString();
		if (textoItemMisTicket.equals(Textos.MIS_TICKETS)) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_TICKETS.getValue()));
			System.out.println("Click en " + textoItemMisTicket);
			while (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue()))
					.toString()))
				;
			if (Boolean
					.parseBoolean(EFA
							.executeAction(Action.IsElementPresent,
									Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_USENOW.getValue()))
							.toString())
					|| Boolean.parseBoolean(EFA
							.executeAction(Action.IsElementPresent,
									Reutilizable.newElement(EComponents.ID.getValue(), Textos.NO_TIENES_TICKECTS))
							.toString())) {
				msjFinal = Mensajes.SUCCESS;
				if (GlobalData.getData("viAccionBackItem").toUpperCase().equals("SI")) {
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
					
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.COMERCE.getValue()));
					msjFinal = Mensajes.SUCCESS;
				}
			} else {
				msjFinal = "no se encontro el elemento";
			}
		} else {
			msjFinal = "El texto del item no coincide";

		}
		return msjFinal;
	}

	public static String itemBeneficiosTunki() throws Exception {
		String msjFinal = "";

		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		String textoItemBeneficios = EFA
				.executeAction(Action.GetText,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EText.ITEM_BENEFICIOS.getValue()))
				.toString();
		if (textoItemBeneficios.equals(Textos.BENEFICIOS_TUNKI)) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_BENEFICIOS.getValue()));
			System.out.println("Click en " + textoItemBeneficios);
			if (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_BENEFICIOS.getValue()))
					.toString())) {
				if (GlobalData.getData("viAccionBackItem").toUpperCase().equals("SI")) {
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
					EFA.executeAction(Action.Click,
							Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.COMERCE.getValue()));
					msjFinal = Mensajes.SUCCESS;
				}
				if (GlobalData.getData("viAccionContinuar").toUpperCase().equals("SI")) {
					msjFinal = "el flujo aun no esta difinido";
				}
			} else {
				msjFinal = "no se encontro el elemento";
			}
		} else {
			msjFinal = "El texto del item no coincide";
		}

		return msjFinal;
	}

	public static String itemConfiguracion() throws Exception {
		String msjFinal = "";

		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));

		String textoItemConfiguracion = EFA
				.executeAction(Action.GetText,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EText.ITEM_CONFIGURACION.getValue()))
				.toString();

		if (textoItemConfiguracion.equals(Textos.CONFIGURACION)) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SETTING.getValue()));
			System.out.println("Click en " + textoItemConfiguracion);
			if (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_TERM_CONDITION.getValue()))
					.toString())) {
				EFA.executeAction(Action.Click,
						Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_TERM_CONDITION.getValue()));
				EFA.executeAction(Action.Back, null);

			}
			else{
				msjFinal="No se encontro btn terminos y condiciones";
			}
			if (Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_TUTORIAL.getValue()))
					.toString())) {
				EFA.executeAction(Action.Click,
						Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_TUTORIAL.getValue()));
				EFA.executeAction(Action.Back, null);

			}
			if (GlobalData.getData("viAccionBackItem").toUpperCase().equals("SI")) {
				EFA.executeAction(Action.Click,
						Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
				EFA.executeAction(Action.Click,
						Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.COMERCE.getValue()));
				msjFinal = Mensajes.SUCCESS;
			} 
		} else {
			msjFinal = "El texto del item no coincide";
		}

		return msjFinal;

	}

	public static String itemSugerirComercio() throws Exception {
		String msjFinal = "";

		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		String textoItemSugerirComercio = EFA
				.executeAction(Action.GetText,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EText.ITEM_SUGERIR_COMERCIO.getValue()))
				.toString();
		if (textoItemSugerirComercio.equals(Textos.SUGERIR_COMERCIO)) {
			EFA.executeAction(Action.Click,
					Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_RECOMMEND_COMMERCE.getValue()));
			System.out.println("click en " + textoItemSugerirComercio);

			if (!Boolean.parseBoolean(EFA
					.executeAction(Action.IsElementPresent,
							Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_SETTING.getValue()))
					.toString())) {
				EFA.cv_driver.quit();
				msjFinal = Mensajes.SUCCESS;
			}
		} else {
			msjFinal = "El texto del item no coincide";
		}
		return msjFinal;

	}
	
	public static String itemContactanos() throws Exception {
		String msjFinal = "";

		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		String textoItemContactanos = EFA
				.executeAction(Action.GetText,
						Reutilizable.newElement(EComponents.XPATH.getValue(), EText.ITEM_CONTACTANOS.getValue()))
				.toString();
		if (textoItemContactanos.equals(Textos.CONTACTANOS)) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.ITEM_CONTACTANOS.getValue()));
			System.out.println("Click en "+textoItemContactanos);
			EFA.cv_driver.quit();
			msjFinal=Mensajes.SUCCESS;
	
		}
		else{
			msjFinal = "El texto del item no coincide";
		}
		return msjFinal;
		
		
	}

}
