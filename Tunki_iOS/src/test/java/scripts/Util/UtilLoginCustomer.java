package scripts.Util;

import com.everis.Action;
import com.everis.EFA;
import com.everis.Element;
import com.everis.GlobalData;

import scripts.helpers.Reutilizable;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.ETextArea;

public class UtilLoginCustomer {
	public static String loadingScreenLogin() throws Exception{
		String msjFinal="";
		while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_CAMBIAR_CUENTA.getValue())).toString())) {
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_RETRY.getValue())).toString())) {
				return msjFinal = "No hay conexion a internet";
			}
		}
		return msjFinal;
	}
	
	public static String enterYourUserName() throws Exception {
		String msjFinal="";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(),EButtons.BTN_CAMBIAR_CUENTA.getValue()));
		Thread.sleep(1000);
		if(!GlobalData.getData("viEmail").isEmpty()) {
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viEmail"));
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.FORMATO_INCORRECTO.getValue())).toString())) {
			return msjFinal = "Formato incorrecto";
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),EMoreElements.CAMPO_NECESARIO.getValue())).toString())) {
			return msjFinal = "Campo necesario";
		}
		return msjFinal;
	}
	
	public static String loginPersona() throws Exception {
		String msjFinal = "";
		int n=0;
		if(GlobalData.getData("viFlujosLoginPersona").toUpperCase().equals("LOGIN DOS VECES")) {
			for(int i=0;i<2;i++){
				n=0;
				for(int j=0;j<GlobalData.getData("viPassword_2").length();j++) {
					EFA.executeAction(Action.Click,idBoton(GlobalData.getData("viPassword_2").substring(n,n+1)));
					EFA.cs_getTestEvidence("Login",500);
					n++;
				}
				while(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BNN_0.getValue())).toString()));
			}
		}
		n=0;
		for(int i=0;i<GlobalData.getData("viPassword").length();i++) {
			EFA.executeAction(Action.Click,idBoton(GlobalData.getData("viPassword").substring(n,n+1)));
			EFA.cs_getTestEvidence("Login",500);
			n++;
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue())).toString())) {
			return msjFinal = "No muestra listado de comercio";
		}
		return msjFinal;
	}
	
	public static Element idBoton (String numero){
		Element boton = new Element("id","btn_"+numero+"");
		return boton;
	}
}
