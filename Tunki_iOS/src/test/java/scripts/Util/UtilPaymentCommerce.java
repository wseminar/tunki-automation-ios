package scripts.Util;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.everis.Action;
import com.everis.EFA;
import com.everis.GlobalData;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import script.textosTunki.Textos;
import scripts.helpers.ReusableRegistroPersona;
import scripts.helpers.Reutilizable;
import scripts.helpers.UtilitariosErrores;
import scripts.helpers.ValidacionBD;
import scripts.identifiers.EBarButtons;
import scripts.identifiers.EButtons;
import scripts.identifiers.EComponents;
import scripts.identifiers.EMoreElements;
import scripts.identifiers.EText;
import scripts.identifiers.ETextArea;

public class UtilPaymentCommerce {
	
	public static String paymentCommerceTicket() throws Exception{
		String msjFinal = "";
		msjFinal = UtilitariosErrores.validarErrorUps();
		if(!msjFinal.isEmpty())return msjFinal;
		EFA.cs_getTestEvidence("Pantalla Pago Ticket", 800);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_QUANTITY.getValue())).toString())) {
			return msjFinal = "no muestra pantalla compra ticket";
		}
		msjFinal = choicePaymentMethod();
		if(!msjFinal.isEmpty())return msjFinal;
		switch (GlobalData.getData("viTipoIngresoMonto")) {
		case "TECLADO":
			EFA.executeAction(Action.Clear, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_QUANTITY.getValue()));
			EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_QUANTITY.getValue()),GlobalData.getData("viMontoCobro"));
			Reutilizable.hacerTap(279, 327);
			break;
		case "BOTONES":
			
		default:
			break;
		}
		EFA.cs_getTestEvidence("Pantalla ticket", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_BUY.getValue()));
		if(GlobalData.getData("viValidarAhora").toUpperCase().equals("PAGO")) {
			msjFinal = UtilPaymentCommerce.validateTarjeta();
			if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
				msjFinal = "";
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
				EFA.cs_getTestEvidence("pago ticket", 1500);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_BUY.getValue()));
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.BIRD.getValue())).toString()));
				if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().equals(Textos.PAGO_REALIZADO)){
					return msjFinal = EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.LBL_TITTLE.getValue())).toString().trim();
				}
				Thread.sleep(10000);
				if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_USELATER.getValue())).toString())) {
					return msjFinal = "No se realizo el pago";
				}
				EFA.cs_getTestEvidence("Pago Realizado", 1000);
				EFA.cs_getTestEvidence("Pago Realizado", 1000);
				EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
			}else {
				return msjFinal;
			}
		}
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.BIRD.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if(!msjFinal.isEmpty())return msjFinal;
		msjFinal = UtilitariosErrores.validarErrorTickets();
		if(!msjFinal.isEmpty())return msjFinal;
		if(GlobalData.getData("viCanjeTicket").toUpperCase().equals("USAR AHORA")){
			msjFinal = getCodeTicket();
			if(!msjFinal.isEmpty())return msjFinal;
		}
		if(GlobalData.getData("viFlujoTicket").toUpperCase().equals("NO USAR")){
			
		}
		return msjFinal = "";
	}
	
	private static String getCodeTicket() throws Exception {
		String msjFinal = "";
		EFA.cs_getTestEvidence("Pop UP Ganaste Ticket", 800);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EMoreElements.IV_TICKET_GLOBE.getValue())).toString())){
			return msjFinal = "No muestra pop up Ganaste Ticket";
		}
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_USENOW.getValue()));
		Thread.sleep(3000);
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_CODE.getValue())).toString())){
			return msjFinal = "No muestra pantalla QR generado";
		}
		EFA.cs_getTestEvidence("Generacion QR", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.PINLEFT.getValue()));
		EFA.cs_getTestEvidence("Constancia", 1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ACCEPT.getValue()));
		EFA.cs_getTestEvidence("Lista de Comercio",1000);
		return msjFinal;
	}
	 
	public static String payNoCard() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_ADD.getValue()));
		if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_NUMBER.getValue())).toString())){
			msjFinal = "No muestra pantalla Vincula tu tarjeta de debito";
		}
		msjFinal = ReusableRegistroPersona.AgregarTarjeta();
		if(!msjFinal.isEmpty())return msjFinal;
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue()));
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_DESPUES.getValue())).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_DESPUES.getValue()));
		}
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return msjFinal;
		if(!GlobalData.getData("viComercio").isEmpty()) {
			if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue())).toString())) {
				return msjFinal = "No muestra listado de comercios";
			}
			if(!UtilPaymentCommerce.buscarComercio(GlobalData.getData("viComercio"))){
				return msjFinal = "No se encontro el comercio.";
			}
		}
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PAY.getValue())).toString())) {
			EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_PAY.getValue()));
		}
		return msjFinal;
	}
	
	public static String choicePaymentMethod() throws Exception{
		String msjFinal = "";
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(),ETextArea.TXT_QUANTITY.getValue())).toString())){
			Reutilizable.hacerTap(208,429);
		}else {
			Reutilizable.hacerTap(336,335);
		}
		Thread.sleep(3000);
		EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		if(!GlobalData.getData("viElegirMetodoPago").isEmpty()) {
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
			switch (GlobalData.getData("viElegirMetodoPago").toUpperCase()) {
			case "CREDITO":
				UtilPaymentCommerce.EleccionTarjetaCredito();
				break;
			case "DEBITO":
				UtilPaymentCommerce.EleccionTarjetaDebito();
			default:
				break;
			}
		}else {
			Reutilizable.hacerTap(261,159);
			EFA.cs_getTestEvidence("Eleccion Metodo de Pago", 800);
		}
		return msjFinal;
	}
	
	public static boolean buscarComercio(String comercio) throws Exception{
		EFA.cs_getTestEvidence("LISTA DE COMERCIO", 500);
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.BUSCAR_COMERCIO.getValue()),comercio.substring(0, comercio.length()-2));
		Thread.sleep(10000);
		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.BUSCAR_COMERCIO.getValue()), comercio.substring(comercio.length()-2, comercio.length()));
		while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		List<WebElement> lista = EFA.cv_driver.findElements(By.id("txt_name"));
		for (WebElement item : lista) {
			if(item.getText().equals(comercio)){
				if(GlobalData.getData("viDetalleComercio").equals("SI")) {
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.IV_INFO.getValue()));
					Reutilizable.tomarCapturaPantalla(300, "Detalle Comercio");
					EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.XPATH.getValue(), EButtons.BTN_PAY.getValue()));
					while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
					Thread.sleep(500);
					EFA.cs_getTestEvidence("Espera Cobro", 0);
					return true;
				}
				item.click(); 
				while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
				Thread.sleep(500);
				EFA.cs_getTestEvidence("Espera Cobro", 0);
				return true;
			}
		}
		return false;
	}

	public static void EleccionTarjetaCredito() throws Exception {
		Reutilizable.hacerSwipe(310,650,-6,-102);
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Eleccion tarjeta", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
	}
	
	public static void EleccionTarjetaDebito() throws Exception {
		Reutilizable.hacerSwipe(351, 632, 3, -44);
		Thread.sleep(500);
		EFA.cs_getTestEvidence("Eleccion tarjeta", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ACEPTAR.getValue()));
	}
	
	public static String menuTicket() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MORE.getValue()));
		EFA.cf_getTestEvidenceWithStep("Menu mas", 800);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.ITEM_TICKETS.getValue()));
		EFA.cs_getTestEvidence("Ticket", 800);
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_EMPTY.getValue())).toString())){
			if(!EFA.executeAction(Action.GetText, Reutilizable.newElement(EComponents.ID.getValue(), EText.TV_EMPTY.getValue())).toString().toUpperCase().equals(Textos.NO_TIENES_TICKECTS.toUpperCase())){
				return msjFinal = "Texto: "+Textos.NO_TIENES_TICKECTS+" diferente";
			}
		}
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_USENOW.getValue())).toString())){
			msjFinal = "";
		}
		if (!msjFinal.isEmpty())return msjFinal;
		EFA.cs_getTestEvidence("Menu Ticket", 1000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BACK_BLUE.getValue()));
		Thread.sleep(800);
		EFA.cs_getTestEvidence("Menu Mas", 800);
		return msjFinal;
	}
	
	public static String menuMove() throws Exception{
		String msjFinal = "";
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EBarButtons.MOVES.getValue()));
		while (Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent,Reutilizable.newElement(EComponents.XPATH.getValue(), EMoreElements.PROGRESS.getValue())).toString()));
		msjFinal = UtilitariosErrores.validarErrorUps();
		if (!msjFinal.isEmpty())return msjFinal;
		EFA.cs_getTestEvidence("Movimientos", 1000);
		return msjFinal;
	}
	
	public static String validateTarjeta() throws Exception {
		String msjFinal = "";
		EFA.cs_getTestEvidence("Pantalla Validacion", 3000);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_VALIDAR_AHORA.getValue()));
		EFA.cs_getTestEvidence("Pantalla Generar Glosa", 1500);
		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_EMPEZAR.getValue()));
		Thread.sleep(20000);
	    while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
	    msjFinal = Reutilizable.validarErrores();
	    if(!msjFinal.isEmpty())return msjFinal;
	    if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue())).toString())) {
	    		return msjFinal = "No muestra pantalla Ingresa el codigo de 6 digitos";
	    }
	    String codigoValidacion = ValidacionBD.obtenerCodigoValidacionTC(GlobalData.getData("viEmail").toUpperCase());
	    System.out.println(codigoValidacion);
	    EFA.cs_getTestEvidence("Pantalla Colocar Codigo", 1000);
	    if(!GlobalData.getData("viCodigoTC").isEmpty()) {
	    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viCodigoTC"));
	    		TouchAction p = new TouchAction((PerformsTouchActions) EFA.cv_driver);
	    		p.tap(304,181).perform();
	    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDAR.getValue()));
	    	    if(GlobalData.getData("viBloquearUsuario").equals("VALIDACION TD")) {
	    	    		for(int i=0;i<2;i++){
		    	    	    while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
		    	    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.REINTENTAR.getValue()));
		    	    		Thread.sleep(3000);
		    	    		EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),GlobalData.getData("viCodigoTC"));
		    	    		p.tap(304,181).perform();
		    	    		EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDAR.getValue()));
	    	    		}
	    	    		String personId = ValidacionBD.obtenerPersonIdxEmail(GlobalData.getData("viEmail"));
	    	    		ValidacionBD.desbloquearUsuario(personId);
	    	    		EFA.cs_getTestEvidence("Cuenta bloqueada", 1500);
	    	    		return msjFinal = "cuenta bloqueada";
	    	    }
	    	    while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
	    	    msjFinal = Reutilizable.validarErrores();
	    	    if(!msjFinal.isEmpty())return msjFinal;
	    	    if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
	    	    		return msjFinal = "no muestra pantalla Tarjeta Validada";
	    	    }
	    }
	    EFA.executeAction(Action.SendKeys, Reutilizable.newElement(EComponents.ID.getValue(), ETextArea.TXT_EMAIL.getValue()),codigoValidacion);
	    EFA.executeAction(Action.Click, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.VALIDAR.getValue()));
	    Thread.sleep(25000);
	    while(Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.XPATH.getValue(),EMoreElements.PROGRESS.getValue())).toString()));	
	    msjFinal = Reutilizable.validarErrores();
	    if(!msjFinal.isEmpty())return msjFinal;   
	    if(!Boolean.parseBoolean(EFA.executeAction(Action.IsElementPresent, Reutilizable.newElement(EComponents.ID.getValue(), EButtons.BTN_OK.getValue())).toString())) {
    		return msjFinal = "no muestra pantalla Tarjeta Validada";
	    }

		return msjFinal;
	}
	
	public static String[] ArrayListofCommerce(){
		String[] xdistancia = new String[6] ;
		List<WebElement> listaElemtoxId = EFA.cv_driver.findElementsById("txt_distance");
		for(int i= 0;i<xdistancia.length;i++){
			for(WebElement webElement : listaElemtoxId){
				String distancia = webElement.getText();
					xdistancia[i] = distancia;
			}
		}
		return xdistancia;
	}
}
